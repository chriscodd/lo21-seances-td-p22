#include "graph.h"


size_t Graph::getNbEdges() const {
	size_t nb = 0;
	for(auto l : adj) {
		nb += l.size();
	}
	return nb;
}

void Graph::addEdge(unsigned int i, unsigned int j) {
	
	if (i >= getNbVertices() || j >= getNbVertices()) {
		throw GraphException("L'un des deux sommets n'existe pas");
	}

	for (unsigned int successeur : adj[i]) {
		if (successeur == j){
			throw GraphException("L'arc existe deja");
		}
	}

	/* Equivalent � :
	for (auto it = adj[i].cbegin(); it != adj[i].cend(); it++) {
		if (*it == j) {
			throw GraphException("L'arc existe d�j�");
		}
	}
	*/

	adj[i].push_back(j);

	/* Equivalent � :
	adj[i].insert(adj[i].cend(), j);
	*/

}

void Graph::removeEdge(unsigned int i, unsigned int j) {
	
	if (i >= getNbVertices() || j >= getNbVertices()) {
		throw GraphException("L'un des deux sommets n'existe pas");
	}

	for (auto it = adj[i].cbegin(); it != adj[i].cend(); it++) {
		if (*it == j) {
			// la m�thode erase retire un �l�ment point� par un iterator donn�
			// c'est l'�quivalent de insert pour ajouter � une position donn�e
			adj[i].erase(it);

			// Si on a trouv�, on retire et on sort
			return;
		}
	}

	/* Equivalent � :
	   La fonction find recherche un �l�ment entre deux iterators
	   Le r�sultat est un iterator qui point sur l'�l�ment s'il est trouv�
	   ou bien sur la borne de "droite" s'il n'est pas trouv�

	auto it2 = find(adj[i].cbegin(), adj[i].cend(), j);
	if (it2 != adj[i].cend()) {
		adj[i].erase(it2);
		return;
	}
	*/
	
	// Si on arrive ici, c'est qu'on n'a pas trouv�
	throw GraphException("L'arc n'existe pas");
	
}

const list<unsigned int>& Graph::getSuccessors(unsigned int i) const {

	if (i >= getNbVertices()) {
		throw GraphException("Le sommet n'existe pas");
	}

	return adj[i];
}

const list<unsigned int> Graph::getPredecessors(unsigned int i) const {
	
	if (i >= getNbVertices()) {
		throw GraphException("Le sommet n'existe pas");
	}
	
	list<unsigned int> predecessors;

	for (size_t sommet = 0; sommet < adj.size(); sommet++) {
		for (auto successeur : adj[sommet]) {
			if (i == successeur) {
				predecessors.push_back(sommet);
				break;
			}
		}
	}

	return predecessors;

}

ostream& operator<<(ostream& f, const Graph& G) {
	f << "graph " << G.getName() << " (" << G.getNbVertices()
		<< " vertices and " << G.getNbEdges() << " edges)" << endl;

	for (unsigned int i = 0; i < G.getNbVertices(); i++) {
		f << i << ": ";
		for (auto j : G.getSuccessors(i)) {
			f << j << " ";
		}
		f << endl;
	}

	f << endl;

	return f;
}
