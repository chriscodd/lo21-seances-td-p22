#include <iostream>
#include "graph.h"

int main() {
    try {
        Graph G1("G1", 5);
        cout << G1;
        G1.addEdge(0, 1);
        G1.addEdge(0, 2);
        G1.addEdge(1, 2);
        G1.addEdge(1, 3);
        G1.addEdge(1, 4);
        G1.addEdge(3, 0);

        cout << G1;

        G1.removeEdge(1, 4);

        cout << G1;

        // Génération d'exceptions
        // G1.addEdge(3, 0);
        // G1.removeEdge(99, 99);
        // G1.addEdge(88, 88);
        // G1.removeEdge(0, 3);

    }
    catch (exception& e) { std::cout << e.what() << "\n"; }

}