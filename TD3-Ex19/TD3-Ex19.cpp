#include <iostream>
#include "fonction.h"

using namespace std;

int& operation(compte*, string);

int main() {

	compte tab[4] = { {"courant", 0},{"codevi", 1500 },{"epargne", 200 }, { "cel", 300 } };

	// On peut modifier le retour de la fonction car c'est une r�f�rence
	// Les modifications sur tab sont donc appliqu�es
	operation(tab, "courant") = 100;
	operation(tab, "codevi") += 100;
	operation(tab, "cel") -= 50;

	for (int i = 0; i < 4; i++) cout << tab[i].id << " : " << tab[i].solde << "\n";

	return 0;
}

int& operation(compte* tabComptes, const string idCompte) {

	// Recherche du compte demand�
	// On fait progresser le pointeur vers le "prochain compte" gr�ce � ++
	// On aurait aussi pu utiliser une boucle for avec un indice
	// Note : dans l'id�al il faudrait g�rer le cas o� idCompte n'existe pas dans tabComptes
	while (tabComptes->id != idCompte) {
		tabComptes++;
	}

	// Renvoi du solde du compte par r�f�rence
	return tabComptes->solde;

	// Remarque importante : la r�f�rence renvoy�e DOIT continuer d'exister
	// en dehors de la fonction (ce qui est notre cas ici)

}
