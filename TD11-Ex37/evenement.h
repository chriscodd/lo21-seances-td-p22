#if !defined(_EVENEMENT_H)
#define _EVENEMENT_H
#include <iostream>
#include <string>
#include <vector>
#include "timing.h"

namespace TIME {
	using namespace std;

	class Evt {
	private:
		std::string sujet;

	protected:
		Evt(const std::string& s) : sujet(s) {}

	public:
		virtual ~Evt() = default;
		const std::string& getDescription() const { return sujet; }
		virtual void afficher(std::ostream& f = std::cout) const = 0;
		virtual Evt* clone() const = 0;
		
		// Fonction permettant de comparer deux �v�nements
		bool operator<(Evt&);

	};
	
	// Fonction permettant d'extraire la date d'un �v�nement,
	// qu'il soit de la famille d'un Evt1j ou de celle d'un EvtPj
	Date getGenericDate(Evt&);

	
	class Evt1j : public Evt  {
	private:
		Date date;
		std::string sujet;

	public:
		Evt1j(const Date& d, const std::string& s) : Evt(s), date(d) {}
		const Date& getDate() const { return date; }
		virtual void afficher(std::ostream& f = std::cout) const override {
			f << "***** Evt ********" << "\n" 
			  << "Date=" << date << " sujet=" << getDescription() << "\n";
		}
		~Evt1j() = default;

		Evt1j* clone() const override {
			return new Evt1j(*this);
		}
	};


	class Evt1jDur : public Evt1j {

		Horaire horaire;
		Duree duree;

	public:
		Evt1jDur(const Date& d, const std::string& s, const Horaire& h, const Duree& dur) :
			Evt1j(d, s), horaire(h), duree(dur) {}

		const Horaire& getHoraire() const { return horaire; }
		const Duree& getDuree() const { return duree;  }

		void afficher(std::ostream& f = std::cout) const override  {
			Evt1j::afficher(f);
			f << "horaire=" << horaire << " duree=" << duree<<"\n";
		}

		~Evt1jDur() = default;
		Evt1jDur* clone() const override {
			return new Evt1jDur(*this);
		}
	};

	class Rdv : public Evt1jDur {
		std::string personnes;
		std::string lieu;
	public:
		Rdv(const Date& d, const std::string& s,
			const Horaire& h, const Duree& dur,
			const std::string& l, const std::string& p) :
			Evt1jDur(d, s,h,dur), personnes(p), lieu(l) {}

		const std::string& getPersonnes() const { return personnes; }
		const std::string& getLieu() const { return lieu; }
		void afficher(std::ostream& f=std::cout) const override  {
			Evt1jDur::afficher(f);
			f << "personnes=" << personnes << " lieu=" << lieu << "\n";
		}
		~Rdv() = default;

		Rdv* clone() const override {
			return new Rdv(*this);
		}
	};

	class EvtPj : public Evt {
	private:
		Date debut;
		Date fin;

	public:
		EvtPj(const Date& d, const Date& f, const std::string& s) : Evt(s), debut(d), fin(f) {}
		~EvtPj() = default;
		Date getDebut() const { return debut; }
		Date getFin() const { return fin; }

		void afficher(std::ostream& f = std::cout) const override {
			f << "***** Evt ********" << "\n";
			f << "Debut=" << debut << " fin=" << fin << " sujet=" << getDescription() << "\n";
		}

		EvtPj* clone() const override {
			return new EvtPj(*this);
		}
	};

	class Agenda {

	private:
		std::vector<Evt*> evts;

	public:
		Agenda() = default;
		Agenda(const Agenda&) = delete;
		Agenda& operator=(const Agenda&) = delete;
		
		Agenda& operator<<(Evt& e) {
			evts.push_back(e.clone());
			return *this;
		}

		virtual ~Agenda() {
			for (auto e : evts) {
				delete e;
			}
		}

		void afficher(std::ostream& f = std::cout) const {
			f << "Agenda ***" << std::endl;

			for (auto e : evts) {
				e->afficher();
			}

			f << "***" << std::endl;
		}

		
		class iterator : public vector<Evt*>::iterator {

		public:

			iterator(const vector<Evt*>::iterator it) : vector<Evt*>::iterator(it) {}

			Evt& operator*() {
				return *(vector<Evt*>::iterator::operator*());
			}
		};

		Agenda::iterator begin() {
			return evts.begin();
		}

		Agenda::iterator end() {
			return iterator(evts.end());
		}

	};
};


#endif