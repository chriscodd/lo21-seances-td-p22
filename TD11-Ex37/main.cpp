
#include <iostream>
#include "evenement.h"

int main() {
	using namespace std;
	using namespace TIME;

	Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Evt1j e2(Date(11, 6, 2013), "Shenzhou");
	Evt1jDur e3(Date(11, 6, 2013), "Lancement de Longue Marche", Horaire(17, 38), Duree(0, 10));
	Rdv e4(Date(11, 4, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "bureau");
	
	Evt1j* pt1 = &e1;
	Evt1j* pt2 = &e2;
	Evt1j* pt3 = &e3;
	Evt1j* pt4 = &e4;
	Evt1j& ref1 = e1;
	Evt1j& ref2 = e2;
	Evt1j& ref3 = e3;
	Evt1j& ref4 = e4;

	cout << "QUESTION 1 --------" << endl;
	
	// On tente de caster avec dynamic cast vers un Rdv*
	// En cas d'�chec, le r�sultat est nullptr
	
	Rdv* pt = dynamic_cast<Rdv*>(pt1);
	if (pt != nullptr) { pt->afficher(); }
	else { cout << "Erreur de cast: pt1" << endl; }

	pt = dynamic_cast<Rdv*>(pt2);
	if (pt != nullptr) { pt->afficher(); }
	else { cout << "Erreur de cast: pt2" << endl; }
	
	pt = dynamic_cast<Rdv*>(pt3);
	if (pt != nullptr) { pt->afficher(); }
	else { cout << "Erreur de cast: pt3" << endl; }

	pt = dynamic_cast<Rdv*>(pt4);
	if (pt != nullptr) { pt->afficher(); }
	else { cout << "Erreur de cast: pt4" << endl; }

	// Pour le dynamic cast de r�f�rences, en cas d'�chec, renvoi d'une exception bad_cast

	try {
		Rdv& r1 = dynamic_cast<Rdv&>(ref1);
		r1.afficher();
	} catch (bad_cast& e) { cout << "Erreur de cast: r1 " << e.what() << endl; }
	
	try {
		Rdv& r2 = dynamic_cast<Rdv&>(ref2);
		r2.afficher();
	}
	catch (bad_cast& e) { cout << "Erreur de cast: r2 " << e.what() << endl; }

	try {
		Rdv& r3 = dynamic_cast<Rdv&>(ref3);
		r3.afficher();
	}
	catch (bad_cast& e) { cout << "Erreur de cast: r3 " << e.what() << endl; }

	try {
		Rdv& r4 = dynamic_cast<Rdv&>(ref4);
		r4.afficher();
	}
	catch (bad_cast& e) { cout << "Erreur de cast: r4 " << e.what() << endl; }


	cout << endl << "QUESTION 2 --------" << endl;

	cout << "e1 < e2 should be true : " << (e1 < e2) << endl;
	cout << "e2 < e3 should be true : " << (e2 < e3) << endl;
	cout << "e3 < e4 should be false : " << (e3 < e4) << endl;
	cout << "e4 < e4 should be false : " << (e4 < e4) << endl;

	
	return 0;
}