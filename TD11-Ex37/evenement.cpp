#include "evenement.h"
#include "timing.h"

using namespace std;
using namespace TIME;

Date TIME::getGenericDate(Evt& e) {
	// En cas d'�v�nement de la famille Evt1j => renvoi getDate()
	try {
		Evt1j& e2 = dynamic_cast<Evt1j&>(e);
		return e2.getDate();
	} catch (bad_cast &) {}

	// En cas d'�v�nement de la famille EvtPj => renvoi getDebut()
	try {
		EvtPj& e2 = dynamic_cast<EvtPj&>(e);
		return e2.getDebut();
	}
	catch (bad_cast &) {}

	throw("Erreur de type dans getGenericDate");

}

bool TIME::Evt::operator<(Evt& e2) {
	if (this == &e2) return false;
	
	Date d1 = getGenericDate(*this);
	Date d2 = getGenericDate(e2);

	if (d1 < d2) return true;
	else if (d2 < d1) return false;
	
	// Les dates sont �gales : il faut tenter de caster en Evt1jDur
	// et comparer les horaires

	Evt1jDur* p1 = dynamic_cast<Evt1jDur*>(this);
	Evt1jDur* p2 = dynamic_cast<Evt1jDur*>(&e2);

	// Si p1 et p2 ont un horaire (cast r�ussi pour les deux)
	if(p1 != nullptr && p2 != nullptr)
		return(p1->getHoraire() < p2->getHoraire());

	// Si p1 a un horaire mais que p2 n'en a pas (hypoth�se: horaire � 00:00)
	if(p1 != nullptr && p2 == nullptr)
		return false;

	// Si p2 a un horaire mais que p1 non
	if (p1 == nullptr && p2 != nullptr)
		return true;

	// Sinon (aucun des deux n'a d'horaire)
	return false;

}
