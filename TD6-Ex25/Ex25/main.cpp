#include <QApplication>
#include <QPushButton>

int main(int argc, char* argv[]) {
    QApplication app(argc, argv);

    // Q1
    QPushButton button("Quitter");
    button.show();

    // Q2 show() est une méthode définie dans QWidgets

    // Q3
    QObject::connect(&button, SIGNAL(clicked()), &app, SLOT(quit()));

    // Q4
    QPushButton button2("Coucou, ceci est un deuxième bouton à déplacer");
    button2.show();

    return app.exec();
}
