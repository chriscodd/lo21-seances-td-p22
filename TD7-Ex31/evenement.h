#ifndef _EVENEMENT_H
#define _EVENEMENT_H

#include <iostream>
#include <string>
#include "timing.h"

using namespace std;

namespace TIME {
	class Evt1j {
		Date date;
		string sujet;

	public:
		Evt1j(const Date& d, const string& s) :date(d), sujet(s) {
			cout << "Construction d'un objet de la classe Evt1j" << endl;
		}
		~Evt1j() {
			cout << "Destruction d'un objet de la classe Evt1j" << endl;
		}

		const string& getSujet() const { return sujet; }
		const Date& getDate() const { return date; }
		void afficher(ostream& f = cout) const {
			f << "***** Evt ********" << "\n" << "Date=" << date << " sujet=" << sujet << "\n";
		}
	};

	class Evt1jDur : public Evt1j {
		Horaire horaire;
		Duree duree;

	public:
		Evt1jDur(const Date& d, const string& s, const Horaire& h, const Duree& dur)
			: Evt1j(d, s), horaire(h), duree(dur) {
			cout << "Construction d'un objet de la classe Evt1jDur" << endl;
		}
		~Evt1jDur() {
			cout << "Destruction d'un objet de la classe Evt1jDur" << endl;
		}

		const Horaire& getHoraire() const { return horaire; }
		const Duree& getDuree() const { return duree; }
		void afficher(ostream& f = cout) const {
			Evt1j::afficher();
			f << "horaire=" << horaire << " duree=" << duree << "\n";
		}
	};

	class Rdv : public Evt1jDur {
		string participants;
		string lieu;

	public:
		Rdv(const Date& d, const string& s, const Horaire& h, const Duree& dur,
			const string& p, const string& l)
			: Evt1jDur(d, s, h, dur), participants(p), lieu(l) {
			cout << "Construction d'un objet de la classe Rdv" << endl;
		}
		~Rdv() {
			cout << "Destruction d'un objet de la classe Rdv" << endl;
		}

		const string& getParticipants() const { return participants; }
		const string& getLieu() const { return lieu; }
		void afficher(ostream& f = cout) const {
			Evt1jDur::afficher();
			f << "participants=" << participants << " lieu=" << lieu << "\n";
		}
	};
}
#endif
