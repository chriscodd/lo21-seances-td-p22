#include <iostream>
#include "evenement.h"

using namespace std;
using namespace TIME;

void afficher2(Evt1j e) {
	e.afficher();
}

int main() {
	
	Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Evt1j e2(Date(11, 6, 2013), "Shenzhou");
	Evt1jDur e3(Date(11, 6, 2013), "Lancement de Longue Marche", Horaire(17, 38), Duree(0, 10));
	Rdv e4(Date(11, 11, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "bureau");

	e1.afficher();
	e2.afficher();
	e3.afficher();
	e4.afficher();

	cout << endl << endl;

	afficher2(e1);
	afficher2(e2);
	afficher2(e3);
	afficher2(e4);

	// A l'appel de la fonction afficher2, on remarque que la m�thode afficher() appel�e est *toujours* celle d'Evt1j.
	// Cela est du au fait que le choix de la m�thode est effectu� *� la compilation* : c'est une ligature statique.
	// Afin d'activer le choix � l'ex�cution, il faut activer le polymorphisme avec le mot cl� virtual sur la m�thode afficher().
	// Attention, cela requiert des ressources compl�mentaires (table de r�f�rence vers toutes les m�thodes virtuelles, 
	// nombreux tests pour conna�tre le type r�el des objets afin d'appeler la "bonne" m�thode afficher()

	return 0;
}