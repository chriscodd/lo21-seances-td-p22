#include <iostream>

using namespace std;

struct essai {
	int n;
	float x;
};

void raz(struct essai* e);
void raz(struct essai& e);

int main() {

	// Cr�ation de 2 structs essai
	struct essai x = { 12, 99.9 };
	struct essai y = { 21, 29.7 };

	// Affichage des 2 structs
	cout << "x : " << x.n << " " << x.x << endl;
	cout << "y : " << y.n << " " << y.x << endl;

	// Remise � z�ro par pointeur de l'une et par r�f�rence de l'autre
	raz(&x);
	raz(y);

	// Affichage des 2 structs
	cout << "x : " << x.n << " " << x.x << endl;
	cout << "y : " << y.n << " " << y.x << endl;

    return 0;
}

void raz(struct essai* e) {
	e->n = 0; // Similaire � (*e).n = 0;
	e->x = 0;
}

void raz(struct essai& e) {
	e.n = 0;
	e.x = 0;
}