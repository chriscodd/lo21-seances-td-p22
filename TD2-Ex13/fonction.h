#ifndef FONCTION_H
#define FONCTION_H

struct essai {
	int n;
	float x;
};

void raz(struct essai& e);
void raz(struct essai* e);

#endif // !FONCTION_H