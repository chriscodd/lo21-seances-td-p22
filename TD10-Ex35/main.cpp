
#include <iostream>
#include "evenement.h"

void ex33() {
	using namespace TIME;
	Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Evt1j e2(Date(11, 6, 2013), "Shenzhou");
	Evt1jDur e3(Date(11, 6, 2013), "Lancement de Longue Marche", Horaire(17, 38), Duree(0, 10));
	Rdv e4(Date(11, 4, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "		bureau");
	e1.afficher(); e2.afficher(); e3.afficher(); e4.afficher();
	Evt1j * pt1 = &e1; Evt1j * pt2 = &e2; Evt1j * pt3 = &e3; Evt1j * pt4 = &e4;
	pt1->afficher(); pt2->afficher(); pt3->afficher(); pt4->afficher();
}

void ex33_2() {
	using namespace TIME;
	Rdv* pt5 = new Rdv(Date(12, 11, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV","bureau");
	pt5->afficher();
	delete pt5;
	Evt1j* pt6 = new Rdv(Date(12, 11, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV","bureau");
	pt6->afficher();
	delete pt6;
}

void ex34() {
	using namespace TIME;
	Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Evt1j e2(Date(11, 6, 2013), "Shenzhou");
	Evt1jDur e3(Date(11, 6, 2013), "Lancement de Longue Marche", Horaire(17, 38), Duree(0, 10));
	Rdv e4(Date(11, 4, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "bureau");
	Agenda A;
	A << e1;
	A << e2 << e3;
	A << e4;
	A.afficher();

}

void ex35() {
	using namespace TIME;
	Evt1j e1(Date(4, 10, 1957), "Spoutnik");
	Evt1j e2(Date(11, 6, 2013), "Shenzhou");
	Evt1jDur e3(Date(11, 6, 2013), "Lancement de Longue Marche", Horaire(17, 38), Duree(0, 10));
	Rdv e4(Date(11, 4, 2013), "reunion UV", Horaire(17, 30), Duree(60), "Intervenants UV", "bureau");
	EvtPj e5(Date(1, 1, 2022), Date(31, 12, 2022), "annee scolaire");
	Agenda A;
	A << e1;
	A << e2 << e3;
	A << e4 << e5;
	A.afficher();

}


int main() {
	using namespace std;
	using namespace TIME;

	ex35();

	return 0;
}