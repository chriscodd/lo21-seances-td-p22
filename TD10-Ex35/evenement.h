#if !defined(_EVENEMENT_H)
#define _EVENEMENT_H
#include <iostream>
#include <string>
#include <vector>
#include "timing.h"

namespace TIME {

	class Evt {
	private:
		std::string sujet;

	protected: // le constructeur est prot�g� pour �tre accessible aux classes fille
		Evt(const std::string& s) : sujet(s) {
			std::cout << "Initialisation des attributs d'un Evt" << std::endl;
		}

	public:
		virtual ~Evt() { std::cout << "destruction Evt " << this << "\n"; }
		const std::string& getDescription() const { return sujet; }
		// La m�thode afficher est virtuelle pure => la classe Evt est abstraite
		virtual void afficher(std::ostream& f = std::cout) const = 0;

	};
	
	class Evt1j : public Evt  {
	private:
		Date date;
		std::string sujet;

	public:
		Evt1j(const Date& d, const std::string& s) : Evt(s), date(d) {
			std::cout << "construction Evt1j " << this << "\n";
		}
		const Date& getDate() const { return date; }
		virtual void afficher(std::ostream& f = std::cout) const override {
			f << "***** Evt ********" << "\n" 
			  << "Date=" << date << " sujet=" << getDescription() << "\n";
		}
		~Evt1j() {
			std::cout << "destruction Evt1j " << this << "\n";
		}
	};


	class Evt1jDur : public Evt1j {

		Horaire horaire;
		Duree duree;

	public:
		Evt1jDur(const Date& d, const std::string& s, const Horaire& h, const Duree& dur) :
			Evt1j(d, s), horaire(h), duree(dur) {
			std::cout << "construction Evt1jDur " << this << "\n";
		}

		const Horaire& getHoraire() const { return horaire; }
		const Duree& getDuree() const { return duree;  }

		void afficher(std::ostream& f = std::cout) const override  {
			//f << "***** Evt ********" << "\n" 
			//<< "Date=" << getDate() << " sujet="
			//<< getDescription() << "\n"
			//<<"horaire="<<horaire<<" duree="<<duree;
			Evt1j::afficher(f); // utilisation de la m�thode
								// de la classe de base
			f << "horaire=" << horaire << " duree=" << duree<<"\n";
		}

		~Evt1jDur() {
			std::cout << "destruction Evt1jDur " << this << "\n";
		}
	};

	class Rdv : public Evt1jDur {
		std::string personnes;
		std::string lieu;
	public:
		Rdv(const Date& d, const std::string& s,
			const Horaire& h, const Duree& dur,
			const std::string& l, const std::string& p) :
			Evt1jDur(d, s,h,dur), personnes(p), lieu(l) {
			std::cout << "construction Rdv " << this << "\n";
		}
		const std::string& getPersonnes() const { return personnes; }
		const std::string& getLieu() const { return lieu; }
		void afficher(std::ostream& f=std::cout) const override  {
			Evt1jDur::afficher(f);
			f << "personnes=" << personnes << " lieu=" << lieu << "\n";
		}
		~Rdv()  {
			std::cout << "destruction Rdv " << this << "\n";
		}
	};

	class EvtPj : public Evt {
	private:
		Date debut;
		Date fin;

	public:
		EvtPj(const Date& d, const Date& f, const std::string& s) : Evt(s), debut(d), fin(f) {
			std::cout << "construction EvtPj " << this << "\n";
		}
		~EvtPj() {
			std::cout << "destruction EvtPj " << this << "\n";
		}
		Date getDebut() const { return debut; }
		Date getFin() const { return fin; }

		void afficher(std::ostream& f = std::cout) const override {
			f << "***** Evt ********" << "\n";
			f << "Debut=" << debut << " fin=" << fin << " sujet=" << getDescription() << "\n";
		}
	};

	class Agenda /* final */ { // si la classe n'est plus sp�cialisable

	private:
		std::vector<Evt*> evts;

	public:
		Agenda() = default;
		// interdiction de la duplication
		Agenda(const Agenda&) = delete;
		Agenda& operator=(const Agenda&) = delete;
		virtual ~Agenda() = default; // si la classe est sp�cialisable
		Agenda& operator<<(Evt& e) {
			evts.push_back(&e);
			return *this;
		}

		void afficher(std::ostream& f = std::cout) const {
			f << "Agenda ***" << std::endl;

			for (auto e : evts) {
				e->afficher();
			}

			f << "***" << std::endl;
		}

	};
}


#endif