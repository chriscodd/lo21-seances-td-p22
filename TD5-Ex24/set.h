#ifndef _SET_H
#define _SET_H

#include <iostream>
#include <string>
#include <initializer_list>
#include <array>
#include <cstdlib>
using namespace std;

namespace Set {
	// classe pour g�rer les exceptions dans le set
	class SetException {
	public:
		SetException(const string& i) :info(i) {}
		string getInfo() const { return info; }
	private:
		string info;
	};

	// caract�ristiques
	enum class Couleur { rouge, mauve, vert };
	enum class Nombre { un = 1, deux = 2, trois = 3 };
	enum class Forme { ovale, vague, losange };
	enum class Remplissage { plein, vide, hachure };

	// conversion d'une caract�ristique en string
	string toString(Couleur c);
	string toString(Nombre v);
	string toString(Forme f);
	string toString(Remplissage v);

	// �criture d'une caract�ristique sur un flux ostream
	ostream& operator<<(ostream& f, Couleur c);
	ostream& operator<<(ostream& f, Nombre v);
	ostream& operator<<(ostream& f, Forme x);
	ostream& operator<<(ostream& f, Remplissage r);

	// listes contenant les valeurs possibles pour chacune des caract�ristiques
	extern std::initializer_list<Couleur> Couleurs;
	extern std::initializer_list<Nombre> Nombres;
	extern std::initializer_list<Forme> Formes;
	extern std::initializer_list<Remplissage> Remplissages;

	// affichage des valeurs possibles pour chaque caract�ristiques
	void printCouleurs(std::ostream& f = cout);
	void printNombres(std::ostream& f = cout);
	void printFormes(std::ostream& f = cout);
	void printRemplissages(std::ostream& f = cout);



	class Carte {

	private:
		Couleur couleur;
		Forme forme;
		Nombre nombre;
		Remplissage remplissage;

		// Q3 : on passe le constructeur de Carte dans la partie priv�e
		// pour emp�cher la cr�ation sauvage de Cartes
		// On d�clare Jeu comme amie pour qu'elle puisse appeler ce constructeur priv�
		Carte(Couleur c, Forme f, Nombre n, Remplissage r) : couleur(c), forme(f), nombre(n), remplissage(r) {}
		friend class Jeu;

	public:
		
		inline Couleur getCouleur() const { return this->couleur; }
		inline Forme getForme() const { return this->forme; }
		inline Nombre getNombre() const { return this->nombre; }
		inline Remplissage getRemplissage() const { return this->remplissage; }

		~Carte() = default;
		Carte(const Carte&) = default;
		Carte& operator=(const Carte&) = default;


	};

	ostream& operator<<(ostream&, const Carte&);



	class Jeu {

	private:
		static Jeu* instanceUnique;
		const Carte* cartes[81];

		Jeu();
		~Jeu();

		// getCarte est maintenant dans la partie priv�e
		// L'id�e est de ne permettre l'acc�s aux cartes que gr�ce � l'iterator
		const Carte& getCarte(size_t) const;

		// On autorise Iterator � acc�der � getCarte
		friend class Iterator;

		// On autorise �galement FormeIterator � acc�der � getCarte
		friend class FormeIterator;


	public:
		static Jeu& donneInstance();
		static void libereInstance();

		Jeu(const Jeu&) = delete;
		Jeu& operator=(const Jeu&) = delete;

		inline size_t getNbCartes() const { return 81; }

		void afficher() const;
		// afficherCartes utilise l'Iterator pour afficher les cartes
		void afficherCartes() const;
		// afficherCartes(Forme) utilise le FormeIterator pour afficher les cartes d'une Forme donn�e
		void afficherCartes(Forme) const;


		// On cr�e une classe encapsul�e dans la classe Jeu
		class Iterator {
		private:
			size_t i = 0;			// Index de parcours des cartes du Jeu
			Iterator() = default;	// On restreint la cr�ation d'un Iterator
			friend class Jeu;		// uniquement accessible � la classe Jeu

		public:
			void next();
			bool isDone() const;
			const Carte& currentItem() const;

		};

		// M�thode publique permettant de demander un Jeu::Iterator � la classe Jeu
		Iterator getIterator() const { return Iterator(); }


		// La classe FormeIterator est similaire � Iterator
		// On a une contrainte suppl�mentaire sur la Forme
		// Le constructeur doit "avancer" dans l'ensemble des cartes jusqu'� la premi�re carte de la Forme f
		// La m�thode next() doit bien entendu en faire de m�me
		class FormeIterator {
		private:
			size_t i = 0;
			Forme forme;
			FormeIterator(Forme);
			friend class Jeu;

		public:
			void next();
			bool isDone() const;
			const Carte& currentItem() const;
			
		};

		// M�thode publique permettant de demander un Jeu::FormeIterator � la classe Jeu
		FormeIterator getFormeIterator(Forme f) const { return FormeIterator(f); }

	};




	class Pioche {

	private :
		const Carte** cartes = nullptr;
		size_t nb = 0;

	public:
		explicit Pioche();
		~Pioche();
		Pioche(const Pioche&) = delete;
		Pioche& operator=(const Pioche&) = delete;

		inline size_t getNbCartes() const { return nb; }
		inline bool estVide() const { return nb == 0; }
		const Carte& piocher();

		void afficher() const;

	};




	class Plateau {

	private:
		const Carte** cartes = nullptr;
		size_t nb;
		size_t nbMax;

	public:
		Plateau();
		~Plateau();
		Plateau(const Plateau&);
		Plateau& operator=(const Plateau&);

		void ajouter(const Carte&);
		void retirer(const Carte&);
		inline size_t getNbCartes() const { return nb; }
		
		void print(ostream& os = cout) const;


		// Q6 ---------------------
		// La classe iterator (ou dans notre cas const_iterator) a un fonctionnement similaire � notre pr�c�dent Iterator
		// Le pointeur current remplace l'indice dans le tableau de cartes i
		// L'op�rateur++ joue le r�le de next()
		// L'op�rateur* jour le r�le de current()
		// getIterator() est remplac�e par begin()
		// isDone() est remplac� par le renvoi d'un iterator vers end() et la comparaison en utilisant l'op�rateur !=
		// Notre const_iterator n'a donc pas besoin de stocker l'information de l'atteinte de la fin du parcours (isDone())
		class const_iterator {
		private:
			const Carte** current = nullptr;
			const_iterator(const Carte** c) :current(c) {}
			friend class Plateau;

		public:
			const_iterator& operator++() { current++; return *this; }
			const Carte& operator*() const { return **current; }
			bool operator!=(const_iterator it) const { return current != it.current; }
		
		};

		// A noter que dans la librairie standard, les m�thodes renvoyant explicitement des const_iterator
		// sont nomm�es cbegin() et cend()
		const_iterator begin() const { return const_iterator(cartes); }
		const_iterator end() const { return const_iterator(cartes + nb); }
		

	};

	ostream& operator<<(ostream&, const Plateau&);
	void afficherCartes(const Plateau&);



};

#endif