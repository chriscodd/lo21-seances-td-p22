#include "set.h"

namespace Set {

	std::initializer_list<Couleur> Couleurs = { Couleur::rouge, Couleur::mauve, Couleur::vert };
	std::initializer_list<Nombre> Nombres = { Nombre::un, Nombre::deux, Nombre::trois };
	std::initializer_list<Forme> Formes = { Forme::ovale, Forme::vague, Forme::losange };
	std::initializer_list<Remplissage> Remplissages = { Remplissage::plein, Remplissage::vide, Remplissage::hachure };

	string toString(Couleur c) {
		switch (c) {
		case Couleur::rouge: return "R";
		case Couleur::mauve: return "M";
		case Couleur::vert: return "V";
		default: throw SetException("Couleur inconnue");
		}
	}

	string toString(Nombre v) {
		switch (v) {
		case Nombre::un: return "1";
		case Nombre::deux: return "2";
		case Nombre::trois: return "3";
		default: throw SetException("Nombre inconnue");
		}
	}

	string toString(Forme f) {
		switch (f) {
		case Forme::ovale: return "O";
		case Forme::vague: return "~";
		case Forme::losange: return "\004";
		default: throw SetException("Forme inconnue");
		}
	}

	string toString(Remplissage r) {
		switch (r) {
		case Remplissage::plein: return "P";
		case Remplissage::vide: return "_";
		case Remplissage::hachure: return "H";
		default: throw SetException("Remplissage inconnu");
		}
	}

	std::ostream& operator<<(std::ostream& f, Couleur c) { f << toString(c); return f; }
	std::ostream& operator<<(std::ostream& f, Nombre v) { f << toString(v); return f; }
	std::ostream& operator<<(std::ostream& f, Forme x) { f << toString(x);  return f; }
	std::ostream& operator<<(std::ostream& f, Remplissage r) { f << toString(r); return f; }

	void printCouleurs(std::ostream& f) {
		for (auto c : Couleurs) f << c << " ";
		f << "\n";
	}

	void printNombres(std::ostream& f) {
		for (auto v : Nombres) f << v << " ";
		f << "\n";
	}

	void printFormes(std::ostream& f) {
		for (auto x : Formes) f << x << " ";
		f << "\n";
	}

	void printRemplissages(std::ostream& f) {
		for (auto r : Remplissages) f << r << " ";
		f << "\n";
	}


	ostream& operator<<(ostream& os, const Carte& c) {
		os << "Carte : " << c.getCouleur() << " " << c.getForme()
			<< " " << c.getNombre() << " " << c.getRemplissage();
		return os;
	}



	// METHODES DE LA CLASSE JEU

	Jeu::Jeu() {
		size_t i = 0;
		for (auto c : Couleurs) {
			for (auto f : Formes) {
				for (auto n : Nombres) {
					for (auto r : Remplissages) {
						cartes[i] = new Carte(c, f, n, r);
						i++;
					}
				}
			}
		}
	}

	Jeu::~Jeu() {
		for (size_t i = 0; i < 81; i++) {
			delete this->cartes[i];
		}
	}

	Jeu* Jeu::instanceUnique = nullptr;

	Jeu& Jeu::donneInstance() {
		if (instanceUnique == nullptr) {
			instanceUnique = new Jeu;
		}
		return *instanceUnique;
	}

	void Jeu::libereInstance() {
		delete instanceUnique;
		instanceUnique = nullptr;
	}

	const Carte& Jeu::getCarte(size_t i) const {
		if (i >= 81) throw SetException("Carte inexistante");
		return *cartes[i];
	}

	void Jeu::afficher() const {
		cout << "JEU COMPLET" << endl;
		for (auto c : cartes) {
			cout << *c << endl;
		}
		cout << "-----------" << endl << endl;
	}

	void Jeu::afficherCartes() const {
		cout << "AFFICHER CARTES -- COMPLET" << endl;
		for (Jeu::Iterator it = Jeu::donneInstance().getIterator(); !it.isDone(); it.next())
			std::cout << it.currentItem() << "\n";
		cout << "-----------" << endl << endl;
	}

	void Jeu::afficherCartes(Forme f) const {
		cout << "AFFICHER CARTES -- FORME " << f << endl;
		for (Jeu::FormeIterator it = Jeu::donneInstance().getFormeIterator(f); !it.isDone(); it.next())
			std::cout << it.currentItem() << "\n";
		cout << "-----------" << endl << endl;
	}


	// Q4 ----------- METHODES DE ITERATOR

	// On remarque l'op�rateur de r�solution de port�e ::, utilis� deux fois
	void Jeu::Iterator::next() {
		if (this->isDone()) throw SetException("Iterateur en fin de s�quence");
		i++;
	}

	bool Jeu::Iterator::isDone() const {
		// L'Iterator a fini de parcourir lorsqu'il a atteint le nombre de cartes pr�sent dans le jeu
		return (i == Jeu::donneInstance().getNbCartes());
	}

	const Carte& Jeu::Iterator::currentItem() const {
		// La m�thode getCarte est maintenant priv�e, mais nous est accessible car Iterator est un ami de Jeu
		if (this->isDone()) throw SetException("Iterateur en fin de s�quence");
		return Jeu::donneInstance().getCarte(i);
	}



	// Q5 ----------- METHODES DE FORMEITERATOR

	Jeu::FormeIterator::FormeIterator(Forme f) : forme(f) {
		while (!isDone() && Jeu::donneInstance().getCarte(i).getForme() != this->forme) {
			i++;
		}
	}

	void Jeu::FormeIterator::next() {
		if (this->isDone()) throw SetException("Iterateur en fin de s�quence");
		i++;
			
		while (!isDone() && Jeu::donneInstance().getCarte(i).getForme() != this->forme) {
			i++;
		}
	}

	bool Jeu::FormeIterator::isDone() const {
		// L'Iterator a fini de parcourir lorsqu'il a atteint le nombre de cartes pr�sent dans le jeu
		return (i == Jeu::donneInstance().getNbCartes());
	}

	const Carte& Jeu::FormeIterator::currentItem() const {
		// La m�thode getCarte est maintenant priv�e, mais nous est accessible car FormeIterator est un ami de Jeu
		if (this->isDone()) throw SetException("Iterateur en fin de s�quence");
		return Jeu::donneInstance().getCarte(i);
	}





	// Q3 : On doit modifier le constructeur de la pioche
	// celui ci doit maintenant passer par le singleton pour r�cup�rer le jeu
	// on remplace donc j par donneInstance, partout

	// Q4 : On doit � nouveau modifier le constructeur
	// la m�thode getCarte n'est plus accessible
	// => il faut passer par l'iterator

	Pioche::Pioche() :	cartes(new const Carte* [Jeu::donneInstance().getNbCartes()]),
						nb(Jeu::donneInstance().getNbCartes()) {

		size_t i = 0;
	//	for (; i < Jeu::donneInstance().getNbCartes(); i++) {	// Impossible dor�navant
		for (auto it = Jeu::donneInstance().getIterator();
			!it.isDone();
			it.next(), i++) {
				this->cartes[i] = &(it.currentItem());
		}
	}

	Pioche::~Pioche() {
		delete[] this->cartes;
	}

	const Carte& Pioche::piocher() {
		if (this->nb == 0) throw SetException("Pioche vide !");

		size_t i = rand() % this->nb;
		
		const Carte& c = *(cartes[i]);
		
		cartes[i] = cartes[this->nb - 1];
		this->nb--;

		return c;

	}

	void Pioche::afficher() const {
		cout << "PIOCHE" << endl;
		for (size_t i = 0; i < nb; i++) {
			cout << *cartes[i] << endl;
		}
		cout << "-------------" << endl << endl;
	}



	// METHODES DE LA CLASSE PLATEAU

	Plateau::Plateau() : cartes(new const Carte*[6]), nb(0), nbMax(6) {}

	Plateau::~Plateau() {
		delete[] this->cartes;
	}
	
	
	Plateau::Plateau(const Plateau& p2) : cartes(new const Carte*[p2.nbMax]), nb(p2.nb), nbMax(p2.nbMax) {
		for (size_t i = 0; i < p2.nb; i++) {
			this->cartes[i] = p2.cartes[i];
		}
	}

	Plateau& Plateau::operator=(const Plateau& p2) {
		if (this == &p2) {
			return *this;
		}

		if (this->nbMax < p2.nb) {  
			delete[] this->cartes;
			this->cartes = new const Carte * [p2.nbMax];
			this->nbMax = p2.nbMax;
		}

		for (size_t i = 0; i < p2.nb; i++) {
			this->cartes[i] = p2.cartes[i];
		}
		this->nb = p2.nb;

		return *this;

	}
	

	void Plateau::ajouter(const Carte& c) {
		if (this->nb == this->nbMax) {
			
			const Carte** newTab = new const Carte*[nbMax * 2];
			
			for (size_t i = 0; i < nbMax; i++) {
				newTab[i] = this->cartes[i];
			}
			
			delete[] this->cartes;
			
			this->cartes = newTab;
			nbMax *= 2;
		}
		this->cartes[nb] = &c;
		nb++;
	}


	void Plateau::retirer(const Carte& c) {
		for (size_t i = 0; i < nb; i++) {
			if (cartes[i] == &c) {
				
				while (i < nb - 1) {
					cartes[i] = cartes[i + 1];
					i++;
				}
				
				cartes[nb - 1] = nullptr;
				nb--;
				return;
			}
		}
		
		throw SetException("Carte non presente sur le plateau !");
	}
	

	void Plateau::print(ostream& os) const {
		os << "PLATEAU" << endl;
		for (size_t i = 0; i < nb; i++) {
			os << *cartes[i] << endl;
		}
		os << "-------------" << endl << endl;
	}

	
	void afficherCartes(const Plateau & p) {
		cout << "AFFICHERCARTES DU PLATEAU" << endl;
		for (Plateau::const_iterator it = p.begin(); it != p.end(); ++it)
			cout << *it << "\n";
		cout << "-------------" << endl << endl;
	}

	ostream& operator<<(ostream& os, const Plateau& plat) {
		plat.print(os);
		return os;
	}


}