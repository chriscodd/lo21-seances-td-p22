#include "set.h"
using namespace Set;

int main() {
	try {
		Jeu::donneInstance().afficherCartes();
		Jeu::donneInstance().afficherCartes(Forme::ovale);

		Pioche p;
		for (size_t i = 0; i < 50; i++) {
			p.piocher();
		}

		Plateau plat;
		plat.ajouter(p.piocher());
		plat.ajouter(p.piocher());
		plat.ajouter(p.piocher());
		plat.ajouter(p.piocher());
		plat.ajouter(p.piocher());
		
		afficherCartes(plat);

	}
	catch (SetException& e) {
		std::cout << e.getInfo() << "\n";
	}


	
	return 0;
}