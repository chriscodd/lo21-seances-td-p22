#include <iostream>
#include "stack.h"

using namespace std;

int main() {

	StackObjectAdapter<int> soa;
	liste.afficher();

	// Doit g�n�rer une exception : index hors limites
	try {
		cout << liste[15];
	}
	catch (ContainerException & e) {
		cerr << e.what();
	}

	liste.push_back("Cellier");
	liste.afficher();

	cout << liste[10] << endl;

	cout << liste.pop_back() << endl;


	liste.clear();
	liste.afficher();

	try {
		cout << liste.pop_back() << endl;
	}
	catch (ContainerException & e) {
		cerr << e.what();
	}
	

	return 0;
}