#ifndef _Container_T_H
#define _Container_T_H

#include<string>
#include<stdexcept>

using namespace std;

namespace TD {

	class ContainerException : public std::exception {

	protected:
		std::string info;

	public:
		ContainerException(const std::string& i = "") noexcept :info(i) {}
		const char* what() const noexcept { return info.c_str(); }
		~ContainerException()noexcept {}

	};


	
	template<typename T> class Container {
	protected:
		size_t nbEl;

	public:

		Container(size_t n = 0) : nbEl(n) {}
		virtual ~Container() = default;
		
		// M�thodes qui peuvent �tre d�finies ici, sans conna�tre la structure de donn�es
		size_t size() const { return nbEl; };
		bool empty() const { return nbEl == 0; };

		// M�thodes impossibles � d�finir sans conna�tre la structure de donn�es => VP
		virtual T& element(size_t i) = 0;
		virtual const T & element(size_t i) const = 0;

		// M�thodes pouvant �tre d�finies dans la classe abstraite en se reposant sur element()
		// et en invitant � red�finir dans les classes filles si besoin => V
		virtual T& front();
		virtual const T& front() const;
		virtual T& back();
		virtual const T& back() const;
		
		// M�thodes virtuelles pures d�pendant de l'impl�mentation
		virtual void push_back(const T& x) = 0;
		virtual T& pop_back() = 0;

		// M�thode pouvant �tre d�finie mais l'algo n'est certainement pas optimal => V
		virtual void clear();

		// Bonus
		virtual void afficher() const;

	};


	template<typename T> T& Container<T>::front() {
		if (empty()) throw ContainerException("Container vide");
		return element(0);
	}

	template<typename T> const T& Container<T>::front() const {
		if (empty()) throw ContainerException("Container vide");
		return element(0);
	}

	template<typename T> T& Container<T>::back() {
		if (empty()) throw ContainerException("Container vide");
		return element(nbEl - 1);
	}

	template<typename T> const T& Container<T>::back() const {
		if (empty()) throw ContainerException("Container vide");
		return element(nbEl - 1);
	}

	template<typename T> void Container<T>::clear() {
		while (!empty()) {
			pop_back();
		}
	}

	template<typename T> void Container<T>::afficher() const {
		cout << endl << "*** CONTAINER ***" << endl;
		for (size_t i = 0; i < nbEl; i++) {
			cout << "[" << element(i) << "] ";
		}
		cout << endl;
	}
}
#endif
