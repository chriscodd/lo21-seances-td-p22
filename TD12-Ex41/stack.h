#ifndef _Stack_T_H
#define _Stack_T_H

#include<string>
#include<stdexcept>

#include "vector.h"

using namespace std;
using namespace TD;

namespace TD {

	template<typename T> class StackObjectAdapter {
		
		Vector<T> v;

	public:

		StackObjectAdapter() : v() {}

		bool empty() const { return v.empty(); }
		void push(const T& x) { v.push_back(x); }
		void pop() { v.pop_back(); }
		size_t size() const { return v.size(); }
		T& top() { return v.back(); }
		const T& top() const { return v.back(); }
		void clear() { v.clear(); }

	};


	template<typename T> class StackClassAdapter : private Vector<T> {

	public:

		StackClassAdapter() = default;

		bool empty() const { return Vector<T>::empty(); }
		void push(const T& x) { Vector<T>::push_back(x); }
		void pop() { Vector<T>::pop_back(); }
		size_t size() const { return Vector<T>::size(); }
		T& top() { return Vector<T>::back(); }
		const T& top() const { return Vector<T>::back(); }
		// void clear() { Vector<T>::clear(); }
		// On peut également utiliser l'instruction using
		using Vector<T>::clear;

	};


	template<typename T, typename CONT> class StackContObjectAdapter {

		CONT conteneur;

	public:

		StackContObjectAdapter() : conteneur() {}

		bool empty() const { return conteneur.empty(); }
		void push(const T& x) { conteneur.push_back(x); }
		void pop() { conteneur.pop_back(); }
		size_t size() const { return conteneur.size(); }
		T& top() { return conteneur.back(); }
		const T& top() const { return conteneur.back(); }
		void clear() { conteneur.clear(); }

	};


	template<typename T, typename CONT> class StackContClassAdapter : private CONT {

	public:

		StackContClassAdapter() = default;

		bool empty() const { return CONT::empty(); }
		void push(const T& x) { CONT::push_back(x); }
		void pop() { CONT::pop_back(); }
		size_t size() const { return CONT::size(); }
		T& top() { return CONT::back(); }
		const T& top() const { return CONT::back(); }
		void clear() { CONT::clear(); }

	};


}


#endif
