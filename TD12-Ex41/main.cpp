#include <iostream>
#include "stack.h"

using namespace std;

int main() {

	cout << "SOA *********" << endl << endl;

	StackObjectAdapter<int> soa;

	soa.push(2);
	soa.push(5);
	soa.push(10);
	soa.push(22);
	soa.push(30);

	cout << "Size should be 5 : " << soa.size() << endl;
	cout << "Top should be 30 : " << soa.top() << endl;
	soa.pop();
	cout << "Size should be 4 : " << soa.size() << endl;
	cout << "Top should be 22 : " << soa.top() << endl;

	cout << endl << endl;

	// STACK CLASS ADAPTER

	cout << "SCA *********" << endl << endl;

	StackClassAdapter<int> sca;

	sca.push(2);
	sca.push(5);
	sca.push(10);
	sca.push(22);
	sca.push(30);

	cout << "Size should be 5 : " << sca.size() << endl;
	cout << "Top should be 30 : " << sca.top() << endl;
	sca.pop();
	cout << "Size should be 4 : " << sca.size() << endl;
	cout << "Top should be 22 : " << sca.top() << endl;

	cout << endl << endl;

	// STACK WITH CONT
		
	cout << "Stack with CONT *********" << endl << endl;

	StackContObjectAdapter<int, Vector<int>> scoa;

	scoa.push(2);
	scoa.push(5);
	scoa.push(10);
	scoa.push(22);
	scoa.push(30);

	cout << "Size should be 5 : " << scoa.size() << endl;
	cout << "Top should be 30 : " << scoa.top() << endl;
	scoa.pop();
	cout << "Size should be 4 : " << scoa.size() << endl;
	cout << "Top should be 22 : " << scoa.top() << endl;

	cout << endl << endl;

	// STACK WITH CONT (class adapter)

	cout << "Stack with CONT (class adapter) *********" << endl << endl;

	StackContClassAdapter<int, Vector<int>> scca;

	scca.push(2);
	scca.push(5);
	scca.push(10);
	scca.push(22);
	scca.push(30);

	cout << "Size should be 5 : " << scca.size() << endl;
	cout << "Top should be 30 : " << scca.top() << endl;
	scca.pop();
	cout << "Size should be 4 : " << scca.size() << endl;
	cout << "Top should be 22 : " << scca.top() << endl;




	return 0;
}