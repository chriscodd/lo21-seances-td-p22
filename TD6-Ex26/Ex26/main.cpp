#include <QApplication>
#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    // Q1
    QWidget fenetre;
    fenetre.setFixedSize(250, 120);

    QLabel* nomLabel = new QLabel("Nom", &fenetre);
    nomLabel->move(10, 10);

    QLineEdit* nomLineEdit = new QLineEdit(&fenetre);
    nomLineEdit->move(60, 10);
    nomLineEdit->setFixedWidth(180);

    QPushButton* okButton = new QPushButton("OK", &fenetre);
    okButton->move(10, 60);
    okButton->setFixedWidth(230);

    fenetre.setWindowTitle("Joueur");

    fenetre.show();


    // Q2 : même chose avec les Layouts
    QWidget fenetre2;
    fenetre2.setFixedSize(250, 120);

    QLabel* nomLabel2 = new QLabel("Nom");
    QLineEdit* nomLineEdit2 = new QLineEdit();
    nomLineEdit2->setFixedWidth(180);

    QPushButton* okButton2 = new QPushButton("OK");

    // Paramétrage des Layouts
    QVBoxLayout* vLayout = new QVBoxLayout;
    QHBoxLayout* hLayout = new QHBoxLayout;

    fenetre2.setLayout(vLayout);

    vLayout->addLayout(hLayout);
    vLayout->addWidget(okButton2);

    hLayout->addWidget(nomLabel2);
    hLayout->addWidget(nomLineEdit2);

    fenetre2.setWindowTitle("Joueur2");

    fenetre2.show();



    return app.exec();
}
