#ifndef FONCTION_H
#define FONCTION_H

#include<string>

using namespace std;

namespace MATH {

	class Fraction {
	private:
		int numerateur;
		int denominateur;

		void simplification();

	public:

		// Constructeur avec initialisations
		Fraction(int n = 0, int d = 1) : numerateur(n), denominateur(d) {
			if (d == 0) {
				cout << "ERREUR : DENOMINATEUR NE PEUT ETRE = 0" << endl;
				this->denominateur = 1;
			}
			this->simplification();

			// Q6
			cout << "Creation de l'objet " << this
				<< " : " << this->numerateur << "/" << this->denominateur << endl;
		}

		// Constructeur avec affectations
		// Fraction(int n = 0, int d = 1) { this->numerateur = n; this->denominateur = d; }

		// Constructeur par d�faut si on ne met pas de valeur par d�faut dans le constructeur plus haut
		// Fraction() : numerateur(0), denominateur(1) {}
		// Constructeur par d�faut g�n�r� automatiquement par le compilateur
		// Fraction() = default;

		~Fraction();
		
		inline int getNumerateur() const { return this->numerateur; }
		inline int getDenominateur() const { return this->denominateur; }
		void setFraction(int n, int d);

		Fraction somme(const Fraction&) const;


		// EXERCICE 21 - OPERATEURS

		// L'appel f2 + f3 est transform� en f2.operator+(f3) par le compilateur
		Fraction operator+(const Fraction&) const;

		// M�thode int�ressante car permet d'�crire f2 + 3 par exemple
		// Elle est potentiellement superflue car le compilateur peut transformer implicitement
		// un entier en Fraction (un constructeur prenant un entier existe)
		// Cependant, cette m�thode ne permet pas d'�crire 3 + f2 : le compilateur ne pourra pas
		// transformer en 3.operator+(Fraction), �a n'existe pas dans la librairie standard
		// Il faut alors d�finir une fonction en dehors de la classe (voir plus bas)
		Fraction operator+(const int) const;

		// Version pr�fixe
		Fraction& operator++();

		// Version postfixe
		// L'argument int est fictif mais doit �tre pr�sent
		const Fraction operator++(int);
		
	};
	   	  

	// EN DEHORS DE LA CLASSE

	// Q1 : setFraction peut fonctionner sous forme de fonction
	// Mais doit obligatoirement faire appel � l'accesseur en �criture (setter)
	// void setFraction(Fraction& f, int n, int d) {
	//	 f.setFraction(n, d);
	// }

	// Q5 : peut fonctionner sous forme de fonction
	Fraction somme(const Fraction& f1, const Fraction& f2);

	// Ex21 Q2 : permet d'�crire f1 = 2 + f3
	Fraction operator+(const int, const Fraction&);

	// Ex21 Q4 : permet d'�crire cout << f1 << f2 << ...
	ostream& operator<<(ostream&, const Fraction&);

}


#endif