#include <iostream>
#include "fonction.h"

using namespace std;
using namespace MATH;


Fraction* myFunction() {
	Fraction fx(7, 8);
	Fraction* pfy = new Fraction(2, 3);
	return pfy;
}


int main() {
	
	// -------- Q1
	Fraction f1;

	/* Inaccessible � partir de Q2 : les attributs ne sont plus publics
	f1.numerateur = 3;
	f1.denominateur = 4;
	*/
	

	// -------- Q2

	f1.setFraction(10, 0);
	cout << f1.getNumerateur() << "/" << f1.getDenominateur() << endl;

	f1.setFraction(10, 5);
	cout << f1.getNumerateur() << "/" << f1.getDenominateur() << endl;

	
	// -------- Q3

	Fraction f2(4, 3);
	cout << f2.getNumerateur() << "/" << f2.getDenominateur() << endl;

	Fraction f3(4, 0);
	cout << f3.getNumerateur() << "/" << f3.getDenominateur() << endl;


	// -------- Q4

	Fraction f4(10, 5);
	cout << f4.getNumerateur() << "/" << f4.getDenominateur() << endl;

	
	// -------- Q5

	Fraction f5 = f2.somme(f3);
	cout << f5.getNumerateur() << "/" << f5.getDenominateur() << endl;

	Fraction f6 = somme(f2, f3);
	cout << f6.getNumerateur() << "/" << f6.getDenominateur() << endl;


	// -------- Q6

	{
		cout << endl << endl << endl;
		cout << " -------- QUESTION 6 -------- " << endl << endl;
		Fraction f1(3, 4);
		Fraction f2(1, 6);
		Fraction* pf3 = new Fraction(1, 2);
		cout << "ouverture d�un bloc\n";
		Fraction* pf6;
		{
			Fraction f4(3, 8);
			Fraction f5(4, 6);
			pf6 = new Fraction(1, 3);
		}
		cout << "fin d�un bloc\n";
		cout << "debut d�une fonction\n";
		Fraction* pf7 = myFunction();
		cout << "fin d�une fonction\n";
		cout << "desallocations controlee par l�utilisateur :\n";
		delete pf6;
		delete pf7;
		cout << endl;
		cout << " -------- FIN QUESTION 6 -------- " << endl << endl << endl;
	}


	// -------- EXERCICE 21

	{
		cout << endl << endl << endl;
		cout << " -------- EXERCICE 21 -------- " << endl << endl;

		Fraction f1(3, 4);
		Fraction f2(5, 8);
		Fraction f3 = f1 + f2;

		cout << f3.getNumerateur() << "/" << f3.getDenominateur() << endl;

		Fraction f4 = f1 + 2;
		Fraction f5 = 2 + f1;

		cout << f4 << endl << f5 << endl;

		f4++;
		++f5;

		cout << f4 << endl << f5 << endl;

		cout << endl;
		cout << " -------- FIN EXERCICE 21 -------- " << endl << endl << endl;

	}

	return 0;
}