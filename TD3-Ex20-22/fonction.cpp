#include <iostream>
#include "fonction.h"

using namespace std;
using namespace MATH;


void MATH::Fraction::setFraction(int n, int d) {
    this->numerateur = n;

    if (d == 0) {
        cout << "ERREUR : DENOMINATEUR NE PEUT ETRE = 0" << endl;
        this->denominateur = 1;
    } else {
        this->denominateur = d;
        this->simplification();
    }
    
    // Equivalent � : this->denominateur = (d == 0) ? 1 : d;

}


void MATH::Fraction::simplification() {
    // si le numerateur est 0, le denominateur prend la valeur 1
    if (numerateur == 0) { denominateur = 1; return; }
    /* un denominateur ne devrait pas �tre 0;
    si c�est le cas, on sort de la m�thode */
    if (denominateur == 0) return;
    /* utilisation de l�algorithme d�Euclide pour trouver le Plus Grand Commun
    Denominateur (PGCD) entre le numerateur et le denominateur */
    int a = numerateur, b = denominateur;
    // on ne travaille qu�avec des valeurs positives...
    if (a < 0) a = -a; if (b < 0) b = -b;
    while (a != b) { if (a > b) a = a - b; else b = b - a; }
    // on divise le numerateur et le denominateur par le PGCD=a
    numerateur /= a; denominateur /= a;
    // si le denominateur est n�gatif, on fait passer le signe - au denominateur
    if (denominateur < 0) { denominateur = -denominateur; numerateur = -numerateur; }

}


Fraction MATH::Fraction::somme(const Fraction& f) const {
    return Fraction(
        this->numerateur * f.denominateur + this->denominateur * f.numerateur,
        this->denominateur * f.denominateur
    );
}


MATH::Fraction::~Fraction() {
    cout << "Suppression de l'objet " << this
        << " : " << this->numerateur << "/" << this->denominateur << endl;
}


Fraction MATH::Fraction::operator+(const Fraction& f2) const {
    // On peut calculer � nouveau la somme ou bien appeler la fonction existante
    return this->somme(f2);
}


Fraction MATH::Fraction::operator+(const int n) const {
    Fraction f2(n, 1);
    return *this + f2;
}


Fraction& MATH::Fraction::operator++() {
    this->setFraction(this->numerateur + this->denominateur, this->denominateur);
    // Possible �galement : *this = *this + 1 ==> fait appel � operator+(int)
    return *this;
}


const Fraction MATH::Fraction::operator++(int) {
    Fraction fTemp(this->numerateur, this->denominateur);
    this->setFraction(this->numerateur + this->denominateur, this->denominateur);
    return fTemp;
}


// ------------- FONCTIONS

Fraction MATH::somme(const Fraction& f1, const Fraction& f2) {
    return Fraction(
        f1.getNumerateur() * f2.getDenominateur() + f1.getDenominateur() * f2.getNumerateur(),
        f1.getDenominateur() * f2.getDenominateur()
    );
}


Fraction MATH::operator+(const int n, const Fraction& f) {
    return f + n;
}


ostream& MATH::operator<<(ostream& os, const Fraction& f) {
    os << f.getNumerateur() << "/" << f.getDenominateur();
    return os;
}
