#include "timing.h"
#include "log.h"

using namespace std;

void MyLogObjectAdapter::addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s) {
	// On cr�e l'�v�nement � stocker dans l'agenda
	TIME::Evt1jDur evt(d, s, h, TIME::Duree(0, 0));

	// Si l'agenda n'est pas vide, il faut tester que le dernier �v�nement
	// est bien ant�rieur � l'�v�nement ajouter

	if (ag.begin() != ag.end()) {
		// On se positionne sur le dernier �l�ment de l'agenda
		TIME::Agenda::const_iterator it = ag.end();
		it--;

		const TIME::Evt& dernierE = *it;
		const TIME::Evt1jDur& dernierEvtjDur = dynamic_cast<const TIME::Evt1jDur&>(dernierE);

		// On compare les deux �v�nements
		if (evt < dernierEvtjDur) {
			throw LogError();
		}
	}	

	// Si tout est OK, on ajoute l'�v�nement
	ag << evt;
	
}

void MyLogObjectAdapter::displayLog(std::ostream& f) const {
	f << std::endl << "LOG OBJECT ADAPTER -------------" << std::endl;

	for (const TIME::Evt& e : ag) {
		// On cast les �v�nements de l'agenda en Evt1jDur
		// L'agenda ne peut contenir que des Evt1jDur puisqu'on le g�re nous-m�mes
		auto ref = dynamic_cast<const TIME::Evt1jDur&>(e);

		// On doit r��crire la m�thode d'affichage de l'�v�nement
		// pour retirer la dur�e
		f << ref.getDate() << " : " << ref.getHoraire() << " - " << ref.getDescription() << "\n";

	}

}


void MyLogClassAdapter::addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s) {
	// On cr�e l'�v�nement � stocker dans l'agenda
	TIME::Evt1jDur evt(d, s, h, TIME::Duree(0, 0));

	// Si l'agenda n'est pas vide, il faut tester que le dernier �v�nement
	// est bien ant�rieur � l'�v�nement ajouter
	
	// Ici, en adaptateur de classe, on acc�de directement � begin et end gr�ce � l'h�ritage priv�

	if (this->begin() != this->end()) {
		// On se positionne sur le dernier �l�ment de l'agenda
		TIME::Agenda::const_iterator it = this->end();
		it--;

		const TIME::Evt& dernierE = *it;
		const TIME::Evt1jDur& dernierEvtjDur = dynamic_cast<const TIME::Evt1jDur&>(dernierE);

		// On compare les deux �v�nements
		if (evt < dernierEvtjDur) {
			throw LogError();
		}
	}

	// Si tout est OK, on ajoute l'�v�nement
	// Ici, on appelle directement la m�thode operator<< gr�ce � l'h�ritage priv�
	*this << evt;
	// Equivalent � this->operator<<(evt);

}

void MyLogClassAdapter::displayLog(std::ostream& f) const {
	f << std::endl << "LOG CLASS ADAPTER -------------" << std::endl;

	for (const TIME::Evt& e : *this) {
		// On cast les �v�nements de l'agenda en Evt1jDur
		// L'agenda ne peut contenir que des Evt1jDur puisqu'on le g�re nous-m�mes
		auto ref = dynamic_cast<const TIME::Evt1jDur&>(e);

		// On doit r��crire la m�thode d'affichage de l'�v�nement
		// pour retirer la dur�e
		f << ref.getDate() << " : " << ref.getHoraire() << " - " << ref.getDescription() << "\n";

	}
}
