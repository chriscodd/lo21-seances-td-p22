#ifndef LOG_H
#define LOG_H

#include <iostream>
#include <exception>
#include "timing.h"
#include "evenement.h"


class Log {

public:
	virtual void addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s) = 0;
	virtual void displayLog(std::ostream& f) const = 0;
};

// Dans l'adaptateur d'objet, un Agenda est encapsul� directement
// dans la partie priv�e du Log

class MyLogObjectAdapter : public Log {
private:
	TIME::Agenda ag;

public:
	MyLogObjectAdapter() = default;
	void addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s) override;
	void displayLog(std::ostream& f = std::cout) const override;

};

// Dans l'adaptateur de clsse, pas d'objet Agenda encapsul� mais h�ritage priv�
// Les m�thodes d'Agenda pourront �tre appel�es naturellement par this

class MyLogClassAdapter : public Log, private TIME::Agenda {

public:
	MyLogClassAdapter() = default;
	void addEvt(const TIME::Date& d, const TIME::Horaire& h, const std::string& s) override;
	void displayLog(std::ostream& f = std::cout) const override;

};


class LogError : public std::exception {
};


#endif