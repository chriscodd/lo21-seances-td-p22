
#include <iostream>
#include "evenement.h"
#include "log.h"

int main() {
	using namespace std;
	using namespace TIME;
	
	MyLogObjectAdapter mloa;

	try {
		mloa.addEvt(Date(11, 4, 2013), Horaire(17, 30), "reunion UV");
		mloa.addEvt(Date(11, 6, 2013), Horaire(17, 38), "Lancement de Longue Marche");
		
		mloa.displayLog();
	}
	catch (LogError&) {
		cout << "POSSIBLE CORRUPTION SYSTEME" << endl;
	}


	MyLogClassAdapter mlca;

	try {
		mlca.addEvt(Date(11, 4, 2013), Horaire(17, 30), "reunion UV");
		mlca.addEvt(Date(11, 6, 2013), Horaire(17, 38), "Lancement de Longue Marche");

		mlca.displayLog();
	}
	catch (LogError&) {
		cout << "POSSIBLE CORRUPTION SYSTEME" << endl;
	}
	
	
	return 0;
}