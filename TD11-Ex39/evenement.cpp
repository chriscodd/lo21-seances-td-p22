#include "evenement.h"
#include "timing.h"

using namespace std;
using namespace TIME;

Date TIME::getGenericDate(const Evt& e) {
	try {
		const Evt1j& e2 = dynamic_cast<const Evt1j&>(e);
		return e2.getDate();
	} catch (bad_cast &) {}

	try {
		const EvtPj& e2 = dynamic_cast<const EvtPj&>(e);
		return e2.getDebut();
	}
	catch (bad_cast &) {}

	throw("Erreur de type dans getGenericDate");

}

bool TIME::Evt::operator<(const Evt& e2) const {
	if (this == &e2) return false;
	
	Date d1 = getGenericDate(*this);
	Date d2 = getGenericDate(e2);

	if (d1 < d2) return true;
	else if (d2 < d1) return false;
	
	const Evt1jDur* p1 = dynamic_cast<const Evt1jDur*>(this);
	const Evt1jDur* p2 = dynamic_cast<const Evt1jDur*>(&e2);

	if(p1 != nullptr && p2 != nullptr)
		return(p1->getHoraire() < p2->getHoraire());

	if(p1 != nullptr && p2 == nullptr)
		return false;

	if (p1 == nullptr && p2 != nullptr)
		return true;

	return false;

}
