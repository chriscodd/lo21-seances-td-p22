#include "set.h"
using namespace Set;

int main() {
	try {
		printCouleurs();
		printNombres();
		printFormes();
		printRemplissages();

		// QUESTION 2 : on ne peut pas cr�er de tableau de Cartes car il n'y a pas de constructeur par d�faut
		// (sans argument ou avec des valeurs par d�faut aux arguments)
		// Carte c[10]; = impossible
		// En revanche, on peut cr�er un tableau de pointeurs de Cartes (initialisation possible plus tard)
		// Carte* c[10]; = fonctionne
		// Carte* c = new Carte[10](); = impossible, car cr�ation des 10 cartes sans initialisateur

		Jeu j;
		Pioche p(j);

		j.afficher();
		p.afficher();
		for (size_t i = 0; i < 50; i++) {
			p.piocher();
		}
		p.afficher();

		cout << endl << endl;

		Plateau plat;
		

		plat.ajouter(p.piocher());
		plat.ajouter(p.piocher());
		plat.print();
		Carte c3 = p.piocher();
		plat.ajouter(c3);
		plat.ajouter(p.piocher());
		plat.ajouter(p.piocher());
		plat.ajouter(p.piocher());
		plat.ajouter(p.piocher());
		plat.print();

		plat.retirer(c3);
		plat.print();

		// plat.retirer(c3); // Doit g�n�rer une exception (retrait d'une carte non pr�sente)
		plat.print();

		cout << endl << "PLATEAU p2" << endl;
		Plateau p2(plat);
		p2.print();

		cout << endl << "PLATEAU p2 apres affectation de p1" << endl;
		plat.ajouter(p.piocher());
		p2 = plat;
		p2.print();

		
		cout << endl << "COMBINAISONS" << endl;

		// TODO test des combinaisons pour obtenir un SET (ou non)

	}
	catch (SetException& e) {
		std::cout << e.getInfo() << "\n";
	}


	try {
		cout << endl << endl << " ********* CONTROLEUR" << endl;

		Controleur c;
		c.distribuer();
		cout << c.getPlateau();
		c.distribuer();
		cout << c.getPlateau();
	}
	catch (SetException & e) {
		std::cout << e.getInfo() << "\n";
	}


	
	return 0;
}