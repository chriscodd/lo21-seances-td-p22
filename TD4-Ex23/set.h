#ifndef _SET_H
#define _SET_H

#include <iostream>
#include <string>
#include <initializer_list>
#include <array>
#include <cstdlib>
using namespace std;

namespace Set {
	// classe pour gérer les exceptions dans le set
	class SetException {
	public:
		SetException(const string& i) :info(i) {}
		string getInfo() const { return info; }
	private:
		string info;
	};

	// caractéristiques
	enum class Couleur { rouge, mauve, vert };
	enum class Nombre { un = 1, deux = 2, trois = 3 };
	enum class Forme { ovale, vague, losange };
	enum class Remplissage { plein, vide, hachure };

	// conversion d'une caractéristique en string
	string toString(Couleur c);
	string toString(Nombre v);
	string toString(Forme f);
	string toString(Remplissage v);

	// écriture d'une caractéristique sur un flux ostream
	ostream& operator<<(ostream& f, Couleur c);
	ostream& operator<<(ostream& f, Nombre v);
	ostream& operator<<(ostream& f, Forme x);
	ostream& operator<<(ostream& f, Remplissage r);

	// listes contenant les valeurs possibles pour chacune des caractéristiques
	extern std::initializer_list<Couleur> Couleurs;
	extern std::initializer_list<Nombre> Nombres;
	extern std::initializer_list<Forme> Formes;
	extern std::initializer_list<Remplissage> Remplissages;

	// affichage des valeurs possibles pour chaque caractéristiques
	void printCouleurs(std::ostream& f = cout);
	void printNombres(std::ostream& f = cout);
	void printFormes(std::ostream& f = cout);
	void printRemplissages(std::ostream& f = cout);



	class Carte {

	private:
		Couleur couleur;
		Forme forme;
		Nombre nombre;
		Remplissage remplissage;

	public:
		Carte(Couleur c, Forme f, Nombre n, Remplissage r) : couleur(c), forme(f), nombre(n), remplissage(r) {}

		inline Couleur getCouleur() const { return this->couleur; }
		inline Forme getForme() const { return this->forme; }
		inline Nombre getNombre() const { return this->nombre; }
		inline Remplissage getRemplissage() const { return this->remplissage; }

		// Pas besoin de redéfinir ces trois méthodes
		// Il n'y a pas d'allocation dynamique de mémoire : les méthodes par défaut suffisent
		~Carte() = default;
		Carte(const Carte&) = default;
		Carte& operator=(const Carte&) = default;


	};

	ostream& operator<<(ostream&, const Carte&);



	// QUESTION 3
	// La relation est une composition : Jeu est responsable du cycle de vie des Cartes
	// = les Cartes ne peuvent pas exister sans un Jeu

	class Jeu {

	private:
		const Carte* cartes[81];

	public:
		Jeu();
		~Jeu();

		// On supprime l'opérateur d'affectation et le constructeur de recopie
		// pour maintenir le lien de composition entre le Jeu et ses Cartes
		// En effet, par défaut, si on copie un objet Jeu, on copie son tableau de pointeurs de Cartes
		// mais les Cartes elles-mêmes ne seront pas dupliquées !
		Jeu(const Jeu&) = delete;
		Jeu& operator=(const Jeu&) = delete;

		inline size_t getNbCartes() const { return 81; }

		const Carte& getCarte(size_t) const;

		void afficher() const;

	};



	// QUESTION 6
	// La relation est une agrégation : la Pioche n'a pas droit de vie ou de mort sur les Cartes

	class Pioche {

	private :
		const Carte** cartes = nullptr;
		size_t nb = 0;

	public:
		// On empêche une conversion implicite par le compilateur d'un Jeu vers une Pioche
		explicit Pioche(const Jeu&);
		~Pioche();
		Pioche(const Pioche&) = delete;
		Pioche& operator=(const Pioche&) = delete;

		inline size_t getNbCartes() const { return nb; }
		inline bool estVide() const { return nb == 0; }
		const Carte& piocher();

		void afficher() const;

	};



	// QUESTION 8
	// La relation est une agrégation

	class Plateau {

	private:
		const Carte** cartes = nullptr;
		size_t nb;
		size_t nbMax;

	public:
		Plateau();
		~Plateau();
		Plateau(const Plateau&);
		Plateau& operator=(const Plateau&);

		void ajouter(const Carte&);
		void retirer(const Carte&);
		inline size_t getNbCartes() const { return nb; }
		
		void print(ostream& os = cout) const;

	};

	ostream& operator<<(ostream&, const Plateau&);


	// QUESTION 10
	// 

	class Combinaison {

	private:
		const Carte* c1;
		const Carte* c2;
		const Carte* c3;

	public:
		Combinaison(const Carte& _c1, const Carte& _c2, const Carte& _c3) : c1(&_c1), c2(&_c2), c3(&_c3) {}
		Combinaison(const Combinaison&) = default;
		Combinaison& operator=(const Combinaison&) = default;
		~Combinaison() = default;

		inline const Carte& getCarte1() const { return *c1; }
		inline const Carte& getCarte2() const { return *c2; }
		inline const Carte& getCarte3() const { return *c3; }

		bool estUnSET() const;

	};

	ostream& operator<<(ostream&, const Combinaison&);



	// QUESTION 12
	// 

	class Controleur {

	private:
		Jeu jeu;
		Pioche* pioche = nullptr;
		Plateau plateau;

	public:
		Controleur();
		void distribuer();
		~Controleur() { delete pioche; }
		inline const Plateau& getPlateau() { return plateau; }

	};



}

#endif