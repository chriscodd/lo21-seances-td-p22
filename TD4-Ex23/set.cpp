#include "set.h"

namespace Set {

	std::initializer_list<Couleur> Couleurs = { Couleur::rouge, Couleur::mauve, Couleur::vert };
	std::initializer_list<Nombre> Nombres = { Nombre::un, Nombre::deux, Nombre::trois };
	std::initializer_list<Forme> Formes = { Forme::ovale, Forme::vague, Forme::losange };
	std::initializer_list<Remplissage> Remplissages = { Remplissage::plein, Remplissage::vide, Remplissage::hachure };

	string toString(Couleur c) {
		switch (c) {
		case Couleur::rouge: return "R";
		case Couleur::mauve: return "M";
		case Couleur::vert: return "V";
		default: throw SetException("Couleur inconnue");
		}
	}

	string toString(Nombre v) {
		switch (v) {
		case Nombre::un: return "1";
		case Nombre::deux: return "2";
		case Nombre::trois: return "3";
		default: throw SetException("Nombre inconnue");
		}
	}

	string toString(Forme f) {
		switch (f) {
		case Forme::ovale: return "O";
		case Forme::vague: return "~";
		case Forme::losange: return "\004";
		default: throw SetException("Forme inconnue");
		}
	}

	string toString(Remplissage r) {
		switch (r) {
		case Remplissage::plein: return "P";
		case Remplissage::vide: return "_";
		case Remplissage::hachure: return "H";
		default: throw SetException("Remplissage inconnu");
		}
	}

	std::ostream& operator<<(std::ostream& f, Couleur c) { f << toString(c); return f; }
	std::ostream& operator<<(std::ostream& f, Nombre v) { f << toString(v); return f; }
	std::ostream& operator<<(std::ostream& f, Forme x) { f << toString(x);  return f; }
	std::ostream& operator<<(std::ostream& f, Remplissage r) { f << toString(r); return f; }

	void printCouleurs(std::ostream& f) {
		for (auto c : Couleurs) f << c << " ";
		f << "\n";
	}

	void printNombres(std::ostream& f) {
		for (auto v : Nombres) f << v << " ";
		f << "\n";
	}

	void printFormes(std::ostream& f) {
		for (auto x : Formes) f << x << " ";
		f << "\n";
	}

	void printRemplissages(std::ostream& f) {
		for (auto r : Remplissages) f << r << " ";
		f << "\n";
	}


	// Q1
	ostream& operator<<(ostream& os, const Carte& c) {
		os << "Carte : " << c.getCouleur() << " " << c.getForme()
			<< " " << c.getNombre() << " " << c.getRemplissage() << endl;
		return os;
	}



	// Q5+ METHODES DE LA CLASSE JEU

	Jeu::Jeu() {
		size_t i = 0;
		for (auto c : Couleurs) {
			for (auto f : Formes) {
				for (auto n : Nombres) {
					for (auto r : Remplissages) {
						cartes[i] = new Carte(c, f, n, r);
						i++;
					}
				}
			}
		}
	}

	Jeu::~Jeu() {
		for (size_t i = 0; i < 81; i++) {
			delete this->cartes[i];
		}
	}

	const Carte& Jeu::getCarte(size_t i) const {
		if (i >= 81) throw SetException("Carte inexistante");
		return *cartes[i];
	}

	void Jeu::afficher() const {
		cout << "JEU COMPLET" << endl;
		for (auto c : cartes) {
			cout << *c;
		}
		cout << "-----------" << endl << endl;
	}



	// Q6+ METHODES DE LA CLASSE PIOCHE

	Pioche::Pioche(const Jeu& j) : cartes(new const Carte* [j.getNbCartes()]), nb(j.getNbCartes()) {
		for (size_t i = 0; i < j.getNbCartes(); i++) {
			this->cartes[i] = &(j.getCarte(i));
		}
	}

	Pioche::~Pioche() {
		delete[] this->cartes;
	}

	const Carte& Pioche::piocher() {
		if (this->nb == 0) throw SetException("Pioche vide !");

		// G�n�ration d'un nombre al�atoire entre 0 et nb-1
		size_t i = rand() % this->nb;
		// R�cup�ration de la carte � l'indice i (al�atoire)
		const Carte& c = *(cartes[i]);
		// D�placement du pointeur vers la derni�re carte de la pioche
		// vers la carte qui va �tre pioch�e
		cartes[i] = cartes[this->nb - 1];
		this->nb--;

		return c;

	}

	void Pioche::afficher() const {
		cout << "PIOCHE" << endl;
		for (size_t i = 0; i < nb; i++) {
			cout << *cartes[i];
		}
		cout << "-------------" << endl << endl;
	}



	// Q6+ METHODES DE LA CLASSE PLATEAU

	Plateau::Plateau() : cartes(new const Carte*[6]), nb(0), nbMax(6) {}

	Plateau::~Plateau() {
		delete[] this->cartes;
	}
	
	
	Plateau::Plateau(const Plateau& p2) : cartes(new const Carte*[p2.nbMax]), nb(p2.nb), nbMax(p2.nbMax) {
		for (size_t i = 0; i < p2.nb; i++) {
			this->cartes[i] = p2.cartes[i];
		}
	}

	Plateau& Plateau::operator=(const Plateau& p2) {
		// Traitement du cas extr�me d'affectation sur soi-m�me (ex p1 = p1;)
		if (this == &p2) {
			return *this;
		}

		// Si p2 contient plus de cartes que this ne peut en contenir
		if (this->nbMax < p2.nb) {  
			delete[] this->cartes;
			this->cartes = new const Carte * [p2.nbMax];
			this->nbMax = p2.nbMax;
		}

		// On pourrait r�duire la taille max de this si p2 ne contient que tr�s peu de cartes
		
		// On pourrait passer tous les pointeurs de carte � nullptr, mais ce n'est pas indispensablepasse this->cartes � nullptr

		// On recopie les cartes de p2 dans this
		for (size_t i = 0; i < p2.nb; i++) {
			this->cartes[i] = p2.cartes[i];
		}
		this->nb = p2.nb;

		return *this;

	}
	

	void Plateau::ajouter(const Carte& c) {
		if (this->nb == this->nbMax) {
			// On doit agrandir le tableau de cartes
			// Pour cela, on en cr�e un second
			const Carte** newTab = new const Carte*[nbMax * 2];
			// On recopie un � un les pointeurs de Cartes dans le nouveau tableau
			for (size_t i = 0; i < nbMax; i++) {
				newTab[i] = this->cartes[i];
			}
			// On d�salloue la m�moire d�tenue par le tableau actuel
			delete[] this->cartes;
			// On affecte au super pointeur le nouveau tableau
			this->cartes = newTab;
			nbMax *= 2;
		}
		this->cartes[nb] = &c;
		nb++;
	}


	void Plateau::retirer(const Carte& c) {
		for (size_t i = 0; i < nb; i++) {
			if (cartes[i] == &c) {
				// On a trouv� : il faut d�caller toutes les cartes suivantes
				while (i < nb - 1) {
					cartes[i] = cartes[i + 1];
					i++;
				}
				// On met la derni�re carte � nullptr, plus propre
				cartes[nb - 1] = nullptr;
				nb--;
				return;
			}
		}
		// Si on a "termin�" le for, c'est qu'on n'a pas trouv�
		throw SetException("Carte non presente sur le plateau !");
	}
	

	void Plateau::print(ostream& os) const {
		os << "PLATEAU" << endl;
		for (size_t i = 0; i < nb; i++) {
			os << *cartes[i];
		}
		os << "-------------" << endl << endl;
	}

	ostream& operator<<(ostream& os, const Plateau& plat) {
		plat.print(os);
		return os;
	}




	// Q10+ METHODES DE LA CLASSE COMBINAISON

	bool Combinaison::estUnSET() const {
		bool b = (c1->getCouleur() == c2->getCouleur() && c2->getCouleur() == c3->getCouleur()) ||
			(c1->getCouleur() != c2->getCouleur() && c2->getCouleur() != c3->getCouleur() && c1->getCouleur() != c3->getCouleur());
		
		if (!b) return false;

		b = (c1->getForme() == c2->getForme() && c2->getForme() == c3->getForme()) ||
			(c1->getForme() != c2->getForme() && c2->getForme() != c3->getForme() && c1->getForme() != c3->getForme());

		if (!b) return false;

		b = (c1->getNombre() == c2->getNombre() && c2->getNombre() == c3->getNombre()) ||
			(c1->getNombre() != c2->getNombre() && c2->getNombre() != c3->getNombre() && c1->getNombre() != c3->getNombre());

		if (!b) return false;

		b = (c1->getRemplissage() == c2->getRemplissage() && c2->getRemplissage() == c3->getRemplissage()) ||
			(c1->getRemplissage() != c2->getRemplissage() && c2->getRemplissage() != c3->getRemplissage() && c1->getRemplissage() != c3->getRemplissage());
		
		return b;

	}


	ostream& operator<<(ostream& os, const Combinaison& c) {
		os << "[ " << c.getCarte1() << "] [" << c.getCarte2() << "] [" << c.getCarte3() << "]";
		if (c.estUnSET()) os << " : SET !";
		os << endl;
		return os;
	}

	Controleur::Controleur() {
		this->pioche = new Pioche(this->jeu);		
	}

	void Controleur::distribuer() {
		if (plateau.getNbCartes() < 12) {
			// Le plateau n'est pas rempli : il faut le remplir
			while (!pioche->estVide() && plateau.getNbCartes() < 12) {
				plateau.ajouter(pioche->piocher());
			}
		}
		else {
			// Le plateau �tait rempli, il faut donc ajouter une seule carte
			if (!pioche->estVide()) {
				plateau.ajouter(pioche->piocher());
			}
		}
		
	}

}