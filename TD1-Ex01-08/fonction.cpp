#include<iostream>
#include<string>

#include "fonction.h"

using namespace std;


// EX3 -------------------

void bonjour() {
	cout << "Entrez votre prenom :";
	string prenom;
	cin >> prenom;
	cout << "Bonjour " << prenom << "\n";
}


// EX5 -------------------

/*
 A retirer au profit de iostream et d'une variable const
#include<stdio.h>
#define PI 3.14159
*/

void exerciceA() {
	int r;
	double p, s;
	const auto pi = 3.14159;
	cout << "donnez le rayon entier d'un cercle:";
	cin >> r;
	p = 2 * pi * r;
	s = pi * r * r;
	cout << "le cercle de rayon " << r;
	cout << " a un perimetre de " << p << " et une surface de " << s << endl;
}


// EX8 -------------------

// Deux mani�res coexistent pour d�finir les fonctions dans un namespace

void chinois::bonjour() {
	cout << "nichao\n";
}

namespace anglais {
	void bonjour() {
		cout << "hello\n";
	}
}


