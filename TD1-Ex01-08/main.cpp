#include <iostream>

#include "fonction.h"

using namespace std;

int main() {

    bonjour();
    
    // EX5 -------------------
    exerciceA();

    
    // EX6 -------------------
    double x = 3.14;
    cout << x << endl;

    double y;
    // cout << y << endl; ==> ne peut pas fonctionner, y n'est pas initialisée
    y = 3.14;
    cout << y << endl;

    // Remarque : l'initialisation et l'affectation sont deux opérations différentes
    // on verra plus tard qu'elles appellent deux fonctions différentes des objets ;
    // l'une appelle le constructeur, l'autre appelle l'opérateur =

    
    // EX7 -------------------
    const double pi = 3.14;
    cout << pi << endl;
    // pi = 13.535; ==> on ne peut modifier la valeur de pi car c'est une const

    // Remarque : déclarer int const a et const int a est équivalent ;
    // ce ne sera pas le cas avec les pointeurs et les références, voir ex 10 & 11

    
    // EX8 -------------------

    chinois::bonjour();
    anglais::bonjour();


       
    return 0;
}


