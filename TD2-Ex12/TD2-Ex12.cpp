#include <iostream>

using namespace std;

void inverseCopie(int a, int b);
void inverse(int* a, int* b);
void inverse(int& a, int& b);

int main() {
    int x = 10, y = 35;

    // Affiche x et y
    cout << "x : " << x << " | y : " << y << endl;

    // Call inverse par copie (bonus)
    inverseCopie(x, y);

    // Affiche x et y
    cout << "x : " << x << " | y : " << y << endl;

    // Call inverse par adresse
    inverse(&x, &y);

    // Affiche x et y
    cout << "x : " << x << " | y : " << y << endl;

    // Call inverse par r�f�rence
    inverse(x, y);

    // Affiche x et y
    cout << "x : " << x << " | y : " << y << endl;

    return 0;

}

void inverseCopie(int a, int b) {
    int tmp;
    tmp = b;
    b = a;
    a = tmp;
}

void inverse(int* a, int* b) {
    int tmp;
    tmp = *b;
    *b = *a;
    *a = tmp;
}

void inverse(int& a, int& b) {
    int tmp;
    tmp = b;
    b = a;
    a = tmp;
}
