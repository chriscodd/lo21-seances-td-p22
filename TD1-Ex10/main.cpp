int main() {
	double* pt0 = 0;  // OK mais pr�f�rer nullptr en c++
	double* pt1 = 4096;  // KO : pas les m�mes types
	double* pt2 = (double*)4096;	// OK, conversion d'un entier en adresse,
									// mais tr�s dangereux : on ne sait pas ce qu'il y a � l'adresse 4096 !
	void* pt3 = pt1;  // Fonctionne car void* peut pointer sur tout (+ g�n�rique que double*)
	pt1 = pt3;  // KO : un double* n'est pas plus g�n�rique qu'un void*
	pt1 = (double*)pt3;  // OK mais pr�f�rer static_cast<double*>

	double d1 = 36; // No pb
	const double d2 = 36;  // No pb
	double* pt4 = &d1;  // No pb
	const double* pt5 = &d1;  // No pb
	*pt4 = 2.1;  // OK, d1 prend la valeur 2.1 
	*pt5 = 2.1;  // KO car pointeur const
	pt4 = &d2;  // KO car la zone m�moire est const (const d2) et le pointeur non
	pt5 = &d2;  // OK car pointeur const

	double* const pt6 = &d1;
	pt6 = &d1;  // KO car le pointeur ne peut �tre r�affect� (double* const) = pointeur fid�le
	*pt6 = 2.78;  // OK car m�me si le pointeur est "fid�le", il peut modifier la zone cibl�e

	double* const pt6b = &d2;  // KO car pt6b est un pointeur fid�le, mais pas const
	const double* const pt7 = &d1;  // OK car pointeur fid�le et const
	pt7 = &d1;  // KO car pointeur fid�le
	*pt7 = 2.78;  // KO car pointeur const

	double const* pt8 = &d1;  // OK, �quivalent � const double* = pointeur constant
	pt8 = &d2;  // OK, pas fid�le
	pt8 = &d1;  // OK, pas fid�le
	*pt8 = 3.14;  // KO car pointeur const

}

