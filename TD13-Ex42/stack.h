#ifndef _Stack_T_H
#define _Stack_T_H

#include<string>
#include<stdexcept>

#include "vector.h"

using namespace std;
using namespace TD;

namespace TD {

	template<typename T> class StackObjectAdapter {

		Vector<T> v;

	public:

		StackObjectAdapter() : v() {}

		bool empty() const { return v.empty(); }
		void push(const T& x) { v.push_back(x); }
		void pop() { v.pop_back(); }
		size_t size() const { return v.size(); }
		T& top() { return v.back(); }
		const T& top() const { return v.back(); }
		void clear() { v.clear(); }

		class iterator {
			typename Vector<T>::iterator current;

		public:
			iterator(const typename Vector<T>::iterator& it) : current(it) {}
			T& operator*() { return *current; }
			iterator& operator++() { ++current; return *this; }
			bool operator==(const iterator& it2) { return current == it2.current; }
			bool operator!=(const iterator& it2) { return current != it2.current; }

		};

		iterator begin() { return(iterator(v.begin())); }
		iterator end() { return(iterator(v.end())); }

	};


	template<typename T> class StackClassAdapter : private Vector<T> {

	public:

		StackClassAdapter() = default;

		bool empty() const { return Vector<T>::empty(); }
		void push(const T& x) { Vector<T>::push_back(x); }
		void pop() { Vector<T>::pop_back(); }
		size_t size() const { return Vector<T>::size(); }
		T& top() { return Vector<T>::back(); }
		const T& top() const { return Vector<T>::back(); }
		// void clear() { Vector<T>::clear(); }
		// On peut également utiliser l'instruction using
		using Vector<T>::clear;

		class iterator : public Vector<T>::iterator {
		public:
			iterator() = default;
			iterator(const Vector<T>::iterator& it) : Vector<T>::iterator(it) {}
		};

		iterator begin() { return iterator(Vector<T>::begin()); }
		iterator end() { return iterator(Vector<T>::end()); }

	};


	template<typename T, typename CONT> class StackContObjectAdapter {

		CONT conteneur;

	public:

		StackContObjectAdapter() : conteneur() {}

		bool empty() const { return conteneur.empty(); }
		void push(const T& x) { conteneur.push_back(x); }
		void pop() { conteneur.pop_back(); }
		size_t size() const { return conteneur.size(); }
		T& top() { return conteneur.back(); }
		const T& top() const { return conteneur.back(); }
		void clear() { conteneur.clear(); }

		class iterator {
			typename CONT::iterator current;

		public:
			iterator(const typename CONT::iterator& it) : current(it) {}
			T& operator*() { return *current; }
			iterator& operator++() { ++current; return *this; }
			bool operator==(const iterator& it2) { return current == it2.current; }
			bool operator!=(const iterator& it2) { return current != it2.current; }

		};

		iterator begin() { return(iterator(conteneur.begin())); }
		iterator end() { return(iterator(conteneur.end())); }

	};


	template<typename T, typename CONT> class StackContClassAdapter : private CONT {

	public:

		StackContClassAdapter() = default;

		bool empty() const { return CONT::empty(); }
		void push(const T& x) { CONT::push_back(x); }
		void pop() { CONT::pop_back(); }
		size_t size() const { return CONT::size(); }
		T& top() { return CONT::back(); }
		const T& top() const { return CONT::back(); }
		void clear() { CONT::clear(); }

		class iterator : public CONT::iterator {
		public:
			iterator() = default;
			iterator(const typename CONT::iterator& it) : CONT::iterator(it) {}
		};

		iterator begin() { return iterator(CONT::begin()); }
		iterator end() { return iterator(CONT::end()); }

	};


}


#endif
