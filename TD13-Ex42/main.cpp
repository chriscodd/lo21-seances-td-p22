#include <iostream>
#include "vector.h"
#include "stack.h"
#include "algorithmes.h"

using namespace std;

int main() {

	// QUESTION 1
	cout << "VECTOR ITERATOR *********" << endl;

	Vector<string> liste;

	liste.push_back("salut");
	liste.push_back("c'est");
	liste.push_back("cool");

	for (auto l : liste) {
		cout << "[" << l << "] ";
	}

	cout << endl << endl;


	// STACK OBJECT ADAPTER

	cout << "STACK OBJECT ADAPTER ITERATOR *********" << endl;

	StackObjectAdapter<int> soa;

	soa.push(2);
	soa.push(5);
	soa.push(10);
	soa.push(22);
	soa.push(30);

	for (auto el : soa) {
		cout << "[" << el << "] ";
	}

	cout << endl << endl;


	// STACK OBJECT ADAPTER

	cout << "STACK CLASS ADAPTER ITERATOR *********" << endl;

	StackClassAdapter<int> sca;

	sca.push(2);
	sca.push(5);
	sca.push(10);
	sca.push(22);
	sca.push(30);

	for (auto el : sca) {
		cout << "[" << el << "] ";
	}

	cout << endl << endl;

	
	// STACK WITH CONT (object adapter)

	cout << "Stack with CONT (object adapter) *********" << endl;

	StackContObjectAdapter<int, Vector<int>> scoa;

	scoa.push(2);
	scoa.push(5);
	scoa.push(10);
	scoa.push(22);
	scoa.push(30);
	
	for (auto el : scoa) {
		cout << "[" << el << "] ";
	}
	
	cout << endl << endl;


	// STACK WITH CONT (class adapter)

	cout << "Stack with CONT (class adapter) *********" << endl;

	StackContClassAdapter<int, Vector<int>> scca;

	scca.push(2);
	scca.push(5);
	scca.push(10);
	scca.push(22);
	scca.push(30);
		
	for (auto el : scca) {
		cout << "[" << el << "] ";
	}
	
	cout << endl << endl;


	// QUESTION 2 *************

	cout << endl << "Question 2 ************" << endl << endl;

	auto it1 = minimum_element<Vector<string>::iterator>(liste.begin(), liste.end());

	cout << "L'element minimum dans le vector liste est : " << *it1 << endl;

	// Param�trage explicite superflu car le compilateur peut trouver
	// de lui-m�me : valeur de retour de begin() et end()
	auto it2 = minimum_element<StackObjectAdapter<int>::iterator>(soa.begin(), soa.end());

	cout << "L'element minimum dans la liste soa est : " << *it2 << endl;

	// Param�trage explicite all�g� par l'instruction using c++11
	using Ite = StackContClassAdapter<int, Vector<int>>::iterator;
	auto it3 = minimum_element<Ite>(scca.begin(), scca.end());

	cout << "L'element minimum dans la liste scca est : " << *it3 << endl;

	cout << endl;


	// QUESTION 3 *************

	cout << endl << "Question 3 ************" << endl << endl;

	auto it4 = compare_element(liste.begin(), liste.end(), TD::compareLongueur);

	cout << "L'element le moins long dans le vector liste est : " << *it4 << endl;

	auto it5 = compare_element(liste.begin(), liste.end(), TD::compareAlpha);

	cout << "L'element le moins grand (alphabetique) dans le vector liste est : " << *it5 << endl;

	auto it6 = compare_element<StackObjectAdapter<int>::iterator, bool(*)(int, int)>(soa.begin(), soa.end(), TD::inferieur);

	cout << "L'element le plus petit dans la liste soa est : " << *it6 << endl;

	auto it7 = compare_element(soa.begin(), soa.end(), TD::superieur);

	cout << "L'element le plus grand dans la liste soa est : " << *it7 << endl;

	auto it8 = compare_element(soa.begin(), soa.end(), [](int x, int y) { return (x > y); });

	cout << "L'element le plus grand dans la liste soa est : " << *it8 << endl;
	   	  
	return 0;
}