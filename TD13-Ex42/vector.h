#ifndef _Vector_T_H
#define _Vector_T_H

#include<string>
#include<stdexcept>

#include "container.h"

using namespace std;
using namespace TD;


namespace TD {

	template<typename T> class Vector : public Container<T> {

	private:
		T* tab;
		size_t nbMax;
		void agrandissementCapacite();

	public:
		Vector(size_t = 0, const T& = T());
		~Vector();

		// Op�rateur red�fini
		const T& operator[](size_t i) const { return element(i); }
		T& operator[](size_t i) { return element(i); }

		// M�thodes VP de Container<T> � red�finir
		const T& element(size_t i) const override;
		T& element(size_t i) override;

		void push_back(const T&) override;
		T& pop_back() override;

		class iterator {
			T* current;

		public:
			iterator(T* t) : current(t) {}
			T& operator*() { return *current; }
			iterator& operator++() { current++; return *this; }
			iterator& operator++(int) { iterator save = *this; current++; return save; }
			bool operator==(const iterator& it2) { return current == it2.current; }
			bool operator!=(const iterator& it2) { return current != it2.current; }

		};

		iterator begin() { return(iterator(tab)); }
		iterator end() { return(iterator(tab + this->nbEl)); }

	};


	template<typename T> Vector<T>::Vector(size_t taille_init, const T& val_init)
		: Container<T>(taille_init), tab(new T[taille_init]), nbMax(taille_init) {

		// Initialisation avec la valeur init
		for (size_t i = 0; i < taille_init; i++) {
			tab[i] = val_init;
		}
	}

	template<typename T> Vector<T>::~Vector() {
		delete[](tab);
	}

	template<typename T> const T& Vector<T>::element(size_t i) const {
		if (i >= this->nbEl) { throw ContainerException("IndexOutOfBound"); }
		return tab[i];
	}

	template<typename T> T& Vector<T>::element(size_t i) {
		if (i >= this->nbEl) { throw ContainerException("IndexOutOfBound"); }
		return tab[i];
	}

	template<typename T> void Vector<T>::push_back(const T& x) {
		if (this->nbEl == nbMax) { agrandissementCapacite(); }

		tab[this->nbEl++] = x;

	}

	template<typename T> T& Vector<T>::pop_back() {
		if (this->nbEl == 0) { throw ContainerException("Tentative de pop sur un vector vide"); }
		return tab[--this->nbEl];
	}


	template<typename T> void Vector<T>::agrandissementCapacite() {
		T* newTab = new T[nbMax + 5];

		for (size_t i = 0; i < nbMax; i++) {
			newTab[i] = tab[i];
		}

		delete[] tab;
		tab = newTab;
		nbMax += 5;

	}

}


#endif