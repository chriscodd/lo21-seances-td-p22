#ifndef _Algorithmes_T_H
#define _Algorithmes_T_H

#include<string>

using namespace std;

namespace TD {
	// Pr�-requis pour la fonction
	// On part du principe que it1 est bien positionn� avant it2

	// Les contraintes impos�es sur le type IT sont :
	// * Un op�rateur < ++ = != == *

	template<typename IT> IT minimum_element(IT it1, IT it2) {
		if (it1 == it2) return it1;

		IT itMin = it1;
		++it1;

		while (it1 != it2) {
			if (*it1 < *itMin) {
				itMin = it1;
			}
			++it1;
		}

		return itMin;
	}

	template<typename IT, typename COMP> IT compare_element(IT it1, IT it2, COMP compare) {
		if (it1 == it2) return it1;

		IT itMin = it1;
		++it1;

		while (it1 != it2) {
			if (compare(*it1, *itMin)) {
				itMin = it1;
			}
			++it1;
		}

		return itMin;
	}


	// Fonctions utilisables pour appliquer une strat�gie � compare_element

	bool compareLongueur(string a, string b) { return a.length() < b.length(); }
	bool compareAlpha(string a, string b) { return a < b; }
	bool superieur(int x, int y) { return x > y; }
	bool inferieur(int x, int y) { return x < y; }

}



#endif