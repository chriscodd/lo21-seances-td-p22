#if !defined(_EVENEMENT_H)
#define _EVENEMENT_H
#include <iostream>
#include <string>
#include <vector>
#include "timing.h"

namespace TIME {
	using namespace std;

	class Evt {
	private:
		std::string sujet;

	protected:
		Evt(const std::string& s) : sujet(s) {}

	public:
		virtual ~Evt() = default;
		const std::string& getDescription() const { return sujet; }
		virtual void afficher(std::ostream& f = std::cout) const = 0;

		// On d�finit une m�thode virtuelle pure clone, qui devra �tre d�finie par les classes filles
		virtual Evt* clone() const = 0;
	};
	
	class Evt1j : public Evt  {
	private:
		Date date;
		std::string sujet;

	public:
		Evt1j(const Date& d, const std::string& s) : Evt(s), date(d) {}
		const Date& getDate() const { return date; }
		virtual void afficher(std::ostream& f = std::cout) const override {
			f << "***** Evt ********" << "\n" 
			  << "Date=" << date << " sujet=" << getDescription() << "\n";
		}
		~Evt1j() = default;

		// Renvoie un Evt1j*, type covariant de Evt* => l'override fonctionne
		Evt1j* clone() const override {
			return new Evt1j(*this);
		}
	};


	class Evt1jDur : public Evt1j {

		Horaire horaire;
		Duree duree;

	public:
		Evt1jDur(const Date& d, const std::string& s, const Horaire& h, const Duree& dur) :
			Evt1j(d, s), horaire(h), duree(dur) {}

		const Horaire& getHoraire() const { return horaire; }
		const Duree& getDuree() const { return duree;  }

		void afficher(std::ostream& f = std::cout) const override  {
			Evt1j::afficher(f); // utilisation de la m�thode
								// de la classe de base
			f << "horaire=" << horaire << " duree=" << duree<<"\n";
		}

		~Evt1jDur() = default;
		Evt1jDur* clone() const override {
			return new Evt1jDur(*this);
		}
	};

	class Rdv : public Evt1jDur {
		std::string personnes;
		std::string lieu;
	public:
		Rdv(const Date& d, const std::string& s,
			const Horaire& h, const Duree& dur,
			const std::string& l, const std::string& p) :
			Evt1jDur(d, s,h,dur), personnes(p), lieu(l) {}

		const std::string& getPersonnes() const { return personnes; }
		const std::string& getLieu() const { return lieu; }
		void afficher(std::ostream& f=std::cout) const override  {
			Evt1jDur::afficher(f);
			f << "personnes=" << personnes << " lieu=" << lieu << "\n";
		}
		~Rdv() = default;

		Rdv* clone() const override {
			return new Rdv(*this);
		}
	};

	class EvtPj : public Evt {
	private:
		Date debut;
		Date fin;

	public:
		EvtPj(const Date& d, const Date& f, const std::string& s) : Evt(s), debut(d), fin(f) {}
		~EvtPj() = default;
		Date getDebut() const { return debut; }
		Date getFin() const { return fin; }

		void afficher(std::ostream& f = std::cout) const override {
			f << "***** Evt ********" << "\n";
			f << "Debut=" << debut << " fin=" << fin << " sujet=" << getDescription() << "\n";
		}

		EvtPj* clone() const override {
			return new EvtPj(*this);
		}
	};

	class Agenda /* final */ { // si la classe n'est plus sp�cialisable

	private:
		std::vector<Evt*> evts;

	public:
		Agenda() = default;
		Agenda(const Agenda&) = delete;
		Agenda& operator=(const Agenda&) = delete;
		
		Agenda& operator<<(Evt& e) {
			// On demande maintenant � l'�v�nement de se cloner
			evts.push_back(e.clone());
			return *this;
		}

		// On doit �crire le destructeur pour lib�rer la m�moire cr��e pour les clones
		virtual ~Agenda() {
			for (auto e : evts) {
				delete e;
			}
		}

		void afficher(std::ostream& f = std::cout) const {
			f << "Agenda ***" << std::endl;

			for (auto e : evts) {
				e->afficher();
			}

			f << "***" << std::endl;
		}

		/***
		 * On pourrait red�finir l'iterator en version STL comme lors des TDs pr�c�dents
		 * Cependant c'est dommage car toutes les m�thodes sont disponibles dans le vector::iterator
		 * On h�rite donc publiquement de vector<>::iterator et on red�finit quelques m�thodes uniquement !
		 * L'avantage est un code moins lourd, plus fiable (car test� +++++ par Microsoft)
		 * L'inconv�nient est une relative complexit� dans la compr�hension de l'architecture
		 */


		class iterator : public vector<Evt*>::iterator {

		public:

			// On d�finit le constucteur de recopie afin de transformer facilement
			// un vector::iterator en Agenda::iterator
			iterator(const vector<Evt*>::iterator it) : vector<Evt*>::iterator(it) {}

			// Pour r�cup�rer un �v�nement (et non un pointeur),
			// on doit red�finir l'op�rateur*, qui appelle celui de la classe m�re
			// et appliquer un niveau d'indirection* suppl�mentaire
			Evt& operator*() {
				return *(vector<Evt*>::iterator::operator*());
			}

			// Les m�thodes operator++, --, !=, ==, etc. sont d�j� d�finies dans la classe m�re !!
		};

		Agenda::iterator begin() {
			return evts.begin();
			// Appel automatique du constructeur de recopie pour transformer en Agenda::iterator
			// => Similaire � return iterator(evts.begin());
		}

		Agenda::iterator end() {
			return iterator(evts.end());
		}

	};
};


#endif