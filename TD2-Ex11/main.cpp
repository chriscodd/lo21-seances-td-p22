int main(){
	
	double& d1 = 36;  // KO : on ne peut pas cr�er de r�f�rence non const depuis une constante
	double d2 = 36;  // RAS
	double& ref = d2;  
	ref = 4;  // OK : on peut modifier la valeur de d2 via la r�f�rence

	const double d3 = 36;
	const double& d4 = 36;
	const double& d5 = d2;

	d5 = 21; // KO : on ne peut pas modifier de valeur via d5 car r�f�rence const

	const double& d6 = d3;
	double& ref2 = d6;  // KO : on ne peut pas cr�er de r�f�rence non const sur une r�f�rence const

	int i = 4;
	double& d7 = i;  // KO : ce ne sont pas les m�mes types

	const double& d8 = i;
	d8 = 3;

}
