#include <iostream>
#include "fonction.h"

using namespace std;

int main() {
    personne p;

    //----------- Q1
    raz(&p);

    //----------- Q2
    affiche_struct(p);

    //----------- Q3
    // 1�re possibilit� : allocation statique d'un tableau de 3 personnes
    personne tabStatique[3] = { { "Jean-Claude", 79 },
                                { "Maryse", 65 },
                                { "Dominique", 32 } };

    // 2�me possibilit� : allocation dynamique d'un tableau de 3 personnes
    personne* tabDynamique = new personne[3];
    tabDynamique[0] = { "Quentin", 17 };
    tabDynamique[1] = { "Sophie", 22 };
    tabDynamique[2] = { "Camille", 15 };

    // Affichage des tableaux
    affiche_tab(tabStatique, 3);
    affiche_tab(tabDynamique, 3);

    //----------- Q4
    init_struct(p, "Michael", 12);
    affiche_struct(p);

    //----------- Q5
    personne p2 = { "Pauline", 25 };
    copy_struct(p, p2);
    affiche_struct(p2);

    //----------- Q6
    cout << endl << endl;
    copy_tab(tabStatique, tabDynamique, 3);
    affiche_tab(tabStatique, 3);
    affiche_tab(tabDynamique, 3);

}