#ifndef FONCTION_H
#define FONCTION_H

struct personne {
	char nom[30];
	unsigned int age;
};

void raz(personne* p);

const void affiche_struct(const personne& p);

const void affiche_tab(const personne* tabP, const int nbP);

void init_struct(personne& p, const char* nom, const int age);

void copy_struct(personne& p, const personne& p2);

void copy_tab(personne* tabP, const personne* tabP2, int nbP);

#endif // !FONCTION_H