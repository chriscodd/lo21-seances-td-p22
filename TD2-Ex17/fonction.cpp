#include<iostream>
#include "fonction.h"

using namespace std;

void raz(personne* p) {
	p->nom[0] = '\0';
	p->age = 0;
}

const void affiche_struct(const personne& p) {
	cout << "Personne : " << p.nom << " a " << p.age << " ans" << endl;
}

const void affiche_tab(const personne* tabP, const int nbP) {
	for (int i = 0; i < nbP; i++) {
		affiche_struct(tabP[i]);
	}
}

void init_struct(personne& p, const char* nom, const int age) {
	int i = 0;
	while (nom[i] != '\0' && i < 30) {
		p.nom[i] = nom[i];
		i++;
	}
	// On n'oublie pas d'ajouter manuellement \0 � la fin de la cha�ne
	p.nom[i] = '\0';

	p.age = age;
}

void copy_struct(personne& p, const personne& p2) {
	init_struct(p, p2.nom, p2.age);
	// ou bien, si passage par adresse : *p = *p2
}

void copy_tab(personne* tabP, const personne* tabP2, int nbP) {
	for (int i = 0; i < nbP; i++) {
		copy_struct(tabP[i], tabP2[i]);
		// �quivalent � : tabP[i] = tabP2[i];
		// �quivalent � : *(tabP + i) = *(tabP2 + i);
	}
}


