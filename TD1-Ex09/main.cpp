#include "fonction.h"

int main() {
    int i = 3, j = 15;
    float x = 3.14159, y = 1.414;
    char c = 'A';
    double z = 3.14159265;

    fct(i); //appel 1 ==> F1
    fct(x); //appel 2 ==> F2
    fct(i, y); //appel 3 ==> F3
    fct(x, j); //appel 4 ==> F4

    fct(c); //appel 5 ==> F1 avec conversion implicite char -> int
    fct((int)c); // appel 5 ==> F1 avec cast A DECONSEILLER
    fct(static_cast<int>(c)); // appel 5 ==> F1 avec cast explicite statique

    // fct(i, j); //appel 6 ==> Ambiguit� entre F3 et F4
    // fct(i, c); //appel 7 ==> Ambiguit� entre F3 et F4

    fct(i, z); //appel 8 ==> F3 avec conversion implicite double -> float
    // fct(z, z); //appel 9 ==> Ambiguit� entre F3 et F4

    // CORRECTION DES APPELS INCORRECTS
    fct(i, static_cast<float>(j)); // appel 6 corrig� ==> F3
    fct(static_cast<float>(i), static_cast<int>(c)); // appel 7 corrig� ==> F4
    fct(static_cast<int>(z), static_cast<float>(z)); // appel 9 corrig�


    // Question 2 ----------------

    // Inliner les fonctions a pour cons�quence de "copier coller" leur code � chaque appel.
    // Cela permet d'acc�l�rer l'ex�cution du programme, car on �vite un changement
    // de contexte (main vers fonction par exemple), op�ration co�teuse si on recherche
    // une forte optimisation.
    // L'inconv�nient est l'augmentation de la taille du code source, et donc de l'ex�cutable.

    // Les fonctions sont "inlin�es" � la compilation.
    // Elles doivent �tre int�gr�es aux .h, pour �tre "visible" par tous les
    // fichiers .cpp qui souhaitent les utiliser
    // Car rappel : les fichiers .cpp sont compil�s s�par�ment,
    // si le code des m�thodes "inlin�es" �tait dans un autre .cpp,
    // il ne serait atteignable qu'� l'�dition de liens (3�me �tape du build)

}

