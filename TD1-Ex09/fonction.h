#ifndef FONCTION_H
#define FONCTION_H

#include <iostream>

inline int fct(int x) { std::cout << "1:" << x << "\n"; return 0; } // F1
inline int fct(float y) { std::cout << "2:" << y << "\n"; return 0; } // F2
inline int fct(int x, float y) { std::cout << "3:" << x << y << "\n"; return 0; } // F3
inline float fct(float x, int y) { std::cout << "4:" << x << y << "\n"; return 3.14; } // F4


#endif

