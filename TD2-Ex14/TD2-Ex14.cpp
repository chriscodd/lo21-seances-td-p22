#include <iostream>

using namespace std;

struct point {
    int x;
    int y;
    int z;
};

/*
Plus n�cessaire car introduction des valeurs par d�faut

void init(point* pt, int _x, int _y, int _z) {
    pt->x = _x; pt->y = _y; pt->z = _z;
}
void init(point* pt, int _x, int _y) {
    pt->x = _x; pt->y = _y; pt->z = 0;
}
void init(point* pt, int _x) {
    pt->x = _x; pt->y = 0; pt->z = 0;
}
void init(point* pt) {
    pt->x = 0; pt->y = 0; pt->z = 0;
}
*/

// Les valeurs par d�faut doivent �tre d�finies dans le prototype de la fonction
void init(point* pt, int _x = 0, int _y = 0, int _z = 0);

// Nul besoin de rappeler les valeurs par d�faut ici
void init(point* pt, int _x, int _y, int _z) {
    pt->x = _x; pt->y = _y; pt->z = _z;
}

int main() {
    point p;

    init(&p);
    cout << p.x << " " << p.y << " " << p.z << endl;
    init(&p, 1);
    cout << p.x << " " << p.y << " " << p.z << endl;
    init(&p, 1, 2);
    cout << p.x << " " << p.y << " " << p.z << endl;
    init(&p, 1, 2, 3);
    cout << p.x << " " << p.y << " " << p.z << endl;

    return 0;
}